-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: hospital
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(30) NOT NULL,
  `content` mediumtext NOT NULL,
  `category_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (1,'测试51','测试15',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(2,'测试52','测试25',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(3,'测试53','测试35',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(4,'测试54','测试45',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(5,'测试55','测试55',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(6,'测试56','测试65',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(7,'测试57','测试75',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(8,'测试58','测试85',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(9,'测试59','测试95',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(10,'测试510','测试105',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(11,'测试511','测试115',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(12,'测试512','测试125',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(13,'测试513','测试135',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(14,'测试514','测试145',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(15,'测试515','测试155',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(16,'测试516','测试165',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(17,'测试517','测试175',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(18,'测试518','测试185',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(19,'测试519','测试195',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(20,'测试520','测试205',5,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(21,'测试61','测试16',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(22,'测试62','测试26',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(23,'测试63','测试36',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(24,'测试64','测试46',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(25,'测试65','测试56',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(26,'测试66','测试66',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(27,'测试67','测试76',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(28,'测试68','测试86',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(29,'测试69','测试96',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(30,'测试610','测试106',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(31,'测试611','测试116',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(32,'测试612','测试126',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(33,'测试613','测试136',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(34,'测试614','测试146',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(35,'测试615','测试156',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(36,'测试616','测试166',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(37,'测试617','测试176',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(38,'测试618','测试186',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(39,'测试619','测试196',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(40,'测试620','测试206',6,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(41,'测试71','测试17',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(42,'测试72','测试27',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(43,'测试73','测试37',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(44,'测试74','测试47',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(45,'测试75','测试57',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(46,'测试76','测试67',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(47,'测试77','测试77',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(48,'测试78','测试87',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(49,'测试79','测试97',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(50,'测试710','测试107',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(51,'测试711','测试117',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(52,'测试712','测试127',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(53,'测试713','测试137',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(54,'测试714','测试147',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(55,'测试715','测试157',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(56,'测试716','测试167',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(57,'测试717','测试177',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(58,'测试718','测试187',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(59,'测试719','测试197',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(60,'测试720','测试207',7,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(61,'测试81','测试18',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(62,'测试82','测试28',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(63,'测试83','测试38',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(64,'测试84','测试48',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(65,'测试85','测试58',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(66,'测试86','测试68',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(67,'测试87','测试78',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(68,'测试88','测试88',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(69,'测试89','测试98',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(70,'测试810','测试108',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(71,'测试811','测试118',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(72,'测试812','测试128',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(73,'测试813','测试138',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(74,'测试814','测试148',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(75,'测试815','测试158',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(76,'测试816','测试168',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(77,'测试817','测试178',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(78,'测试818','测试188',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(79,'测试819','测试198',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(80,'测试820','测试208',8,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(81,'测试91','测试19',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(82,'测试92','测试29',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(83,'测试93','测试39',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(84,'测试94','测试49',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(85,'测试95','测试59',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(86,'测试96','测试69',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(87,'测试97','测试79',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(88,'测试98','测试89',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(89,'测试99','测试99',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(90,'测试910','测试109',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(91,'测试911','测试119',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(92,'测试912','测试129',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(93,'测试913','测试139',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(94,'测试914','测试149',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(95,'测试915','测试159',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(96,'测试916','测试169',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(97,'测试917','测试179',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(98,'测试918','测试189',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(99,'测试919','测试199',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(100,'测试920','测试209',9,1,'2014-11-23 12:31:36','2014-11-23 12:31:36'),(101,'测试101','测试110',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(102,'测试102','测试210',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(103,'测试103','测试310',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(104,'测试104','测试410',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(105,'测试105','测试510',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(106,'测试106','测试610',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(107,'测试107','测试710',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(108,'测试108','测试810',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(109,'测试109','测试910',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(110,'测试1010','测试1010',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(111,'测试1011','测试1110',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(112,'测试1012','测试1210',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(113,'测试1013','测试1310',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(114,'测试1014','测试1410',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(115,'测试1015','测试1510',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(116,'测试1016','测试1610',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(117,'测试1017','测试1710',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(118,'测试1018','测试1810',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(119,'测试1019','测试1910',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(120,'测试1020','测试2010',10,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(121,'测试111','测试111',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(122,'测试112','测试211',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(123,'测试113','测试311',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(124,'测试114','测试411',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(125,'测试115','测试511',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(126,'测试116','测试611',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(127,'测试117','测试711',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(128,'测试118','测试811',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(129,'测试119','测试911',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(130,'测试1110','测试1011',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(131,'测试1111','测试1111',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(132,'测试1112','测试1211',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(133,'测试1113','测试1311',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(134,'测试1114','测试1411',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(135,'测试1115','测试1511',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(136,'测试1116','测试1611',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(137,'测试1117','测试1711',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(138,'测试1118','测试1811',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(139,'测试1119','测试1911',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09'),(140,'测试1120','测试2011',11,1,'2014-11-23 12:34:09','2014-11-23 12:34:09');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campus`
--

DROP TABLE IF EXISTS `campus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(30) NOT NULL,
  `work_time` char(60) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campus`
--

LOCK TABLES `campus` WRITE;
/*!40000 ALTER TABLE `campus` DISABLE KEYS */;
INSERT INTO `campus` VALUES (2,'龙洞','早上九点至下午五点','2014-11-24 13:21:14','2014-11-24 13:18:22'),(3,'大学城','早上九点至下午四点','2014-11-24 13:21:29','2014-11-24 13:21:29'),(4,'东风路','早上九点至下午六点','2014-11-24 13:21:52','2014-11-24 13:21:52');
/*!40000 ALTER TABLE `campus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(30) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `is_parent` int(11) NOT NULL DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'医院概况',0,1,NULL,'2014-11-23 11:51:17'),(2,'医院新闻',0,1,NULL,'2014-11-23 11:51:17'),(3,'健康保健',0,1,NULL,'2014-11-23 11:51:17'),(4,'交流平台',0,1,NULL,'2014-11-23 11:51:17'),(5,'就医指南',0,0,NULL,'2014-11-23 11:51:17'),(6,'科室介绍',0,0,NULL,'2014-11-23 11:51:17'),(7,'公费医疗',0,0,NULL,'2014-11-23 11:51:17'),(8,'预防保健',3,0,NULL,'2014-11-23 11:51:17'),(9,'健康教育',3,0,NULL,'2014-11-23 11:51:17'),(10,'院内',2,0,NULL,'2014-11-23 12:33:18'),(11,'院外',4,0,NULL,'2014-11-23 12:33:23');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` char(30) NOT NULL,
  `description` text NOT NULL,
  `phone` char(20) NOT NULL,
  `campus` char(30) NOT NULL,
  `portrait` text,
  `position` char(50) DEFAULT NULL,
  `expert` int(11) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (1,'GOood','GOOD','5698868','GDUT','/static/upload/images/19850cef2077d3eb1f2e99778993824e.jpg','Programmer',1,'2014-11-24 07:28:19','2014-11-23 12:52:01'),(3,'Gallon','GOOD','15622741509','GDUT','/static/upload/images/Cn4i7im.jpg','Programmer',0,NULL,'2014-11-24 07:18:17');
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(50) NOT NULL,
  `content` text NOT NULL,
  `fullname` char(30) NOT NULL,
  `email` char(60) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `comment` text,
  `handler_id` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `introduce`
--

DROP TABLE IF EXISTS `introduce`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `introduce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(30) NOT NULL,
  `content` mediumtext NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `introduce`
--

LOCK TABLES `introduce` WRITE;
/*!40000 ALTER TABLE `introduce` DISABLE KEYS */;
INSERT INTO `introduce` VALUES (1,'','<p><img alt=\"\" src=\"/static/upload/images/Cn4i7im.jpg\" style=\"height:251px; width:208px\" />GDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT Hospital</p>\n\n<p>GDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT Hospital</p>\n\n<p>GDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT HospitalGDUT Hospital</p>\n',NULL,'2014-11-24 07:42:45');
/*!40000 ALTER TABLE `introduce` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `link`
--

DROP TABLE IF EXISTS `link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `url_link` varchar(256) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `link`
--

LOCK TABLES `link` WRITE;
/*!40000 ALTER TABLE `link` DISABLE KEYS */;
INSERT INTO `link` VALUES (1,'Google','google.com',NULL,'2014-11-24 09:17:40'),(2,'Facebook','facebook.com',NULL,'2014-11-24 09:20:47'),(3,'Twitter','twitter.com',NULL,'2014-11-24 09:20:57'),(4,'Blog','blog.com',NULL,'2014-11-24 09:21:04'),(5,'Gallon','gallon.cc',NULL,'2014-11-24 09:21:10'),(6,'Github','github.com',NULL,'2014-11-24 09:21:23');
/*!40000 ALTER TABLE `link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(30) NOT NULL,
  `password` char(255) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `session` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `session_UNIQUE` (`session`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'root','33a485cb146e1153c69b588c671ab474f2e5b800',NULL,'2014-11-23 11:51:44',NULL),(2,'gallon','069029ae719bff83f8842539ca9b1708ab5a4f62','2014-11-24 14:11:51','2014-11-24 14:11:29','63e6046552285dc938a329158bbb553fb041dd16');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-22 10:10:00
