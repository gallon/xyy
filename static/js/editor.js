CKEDITOR.replace( 'editor1',
	              {
	                  filebrowserBrowseUrl : "/static/ckfinder/ckfinder.html",
	                  filebrowserImageBrowseUrl : "/static/ckfinder/ckfinder.html?Type=Images",
	                  filebrowserFlashBrowseUrl : "/static/ckfinder/ckfinder.html?Type=Flash",
	                  filebrowserUploadUrl : "/static/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files",
	                  filebrowserImageUploadUrl : "/static/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images",
	                  filebrowserFlashUploadUrl : "/static/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash"
	              });

$(document).ready(function() {
    /* Our ckeck-variable which tells us if we need to warn the user before leave or not
     initialized with 'false' so the don't bother the user when we don't have to. */
    var warn_on_leave = false;

    /* Here we check if the user has made any 'changes' actually we just check if he has pressed a key in ckeditor.
     'cause you have to do that it's a proper solution.
     This Code is NOT written in jquery, 'cause I haven't found a proper way to combine jQuery and CKEditor eventhandling
     The first line is an event-listener which is called everytime we change our focus (i.e. click in our ckeditor, a link, etc)
     The second 'CKEDITOR.currentInstance' is a method which returns our currently focused element. Here we listen if the user presses a key.
     The 'try...catch' Block is needed so we don't produce an error when our focus is 'null'. */
    CKEDITOR.on('currentInstance', function() {
        try {
            CKEDITOR.currentInstance.on('key', function() {
                warn_on_leave = true;
            });
        } catch (err) { }
    });


    // We don't want to annoy the user with a popup, when he's about to save his/her changes
    $(document.activeElement).submit(function() {
        warn_on_leave = false;
    });

    // Finally we check if we need to show our popup or not
    $(window).bind('beforeunload', function() {
        if(warn_on_leave) {
            return 'Achtung! Es wurde nicht gespeichert! Alle Eingaben gehen verloren!';
        }
    });
});
