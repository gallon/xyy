$(document).ready(function() {
	$('#delete-button').click(function(event) {
		$('#delete-form').submit();
	});
	$('.category-list .modify-button').click(function(event) {
		var categoryId = $(this).parent("div").children('.category-id').val();
		var categoryTitle = $(this).parent("div").children('.category-title').val();
		$('#modify-category-id').val(categoryId);
		$('#modify-form #category-title').val(categoryTitle);
	});
	$('.category-list .delete-button').click(function(event) {
		var categoryId = $(this).parent("div").children('.category-id').val();
		$('#delete-category-id').val(categoryId);
	});

	$('.campus-list .modify-button').click(function(event) {
		var campusId = $(this).parent("div").children('.campus-id').val();
		var campusTitle = $(this).parent("div").children('.campus-title').val();
		var campusWorkTime = $(this).parent("div").children('.campus-work-time').val();
		$('#modify-form #campus-id').val(campusId);
		$('#modify-form #campus-title').val(campusTitle);
		$('#modify-form #campus-work-time').val(campusWorkTime);
	});
	$('.campus-list .delete-button').click(function(event) {
		var campusId = $(this).parent("div").children('.campus-id').val();
		$('#delete-form #campus-id').val(campusId);
	});
	$('#modify-submit').click(function(event) {
		$('#modify-form').submit();
	});
	$('#user-modify-submit').click(function(event) {
		$('#user-modify-form').submit();
	});
});
