// $(function() {
// 	$('#showPic').slidesjs({
// 	width: 212,
// 	height: 140,
// 	start: 1,
// 	play: {
// 			active: false,
// 			auto: true,
// 			interval: 4000,
// 			swap: true,
// 			restartDelay: 2500
// 		}
// 	});
// });
// $(function(){
// 	var page = 1;
// 	var i = 3;
// 	$("span.RButton").click(function(){
// 		var $parent = $(this).parents("div#EIPic");
		
// 		var $show = $parent.find("div.picContList");
// 		var $picCont = $parent.find("div.picCont");
// 		var vWidth = $picCont.width();
// 		console.log(vWidth);
// 		var len = $show.find("li").length;
// 		console.log();
// 		if(	!$show.is(":animated")	){
// 			if(	page == i	){
// 				$show.animate({left: '0px'}, "fast");
// 				page = 1;
// 			}	else	{
// 				$show.animate({left: '-=' + vWidth / 5}, "slow");
// 				page++;
// 			}
// 		}
// 	});
// 	$("span.LButton").click(function(){
// 		var $parent = $(this).parents("div#EIPic");
// 		var $show = $parent.find("div.picContList");
// 		var $picCont = $parent.find("div.picCont");
// 		var vWidth = $picCont.width();
// 		var len = $show.find("li").length;
// 		if(	!$show.is(":animated")	){
// 			if(	 page == 1	){
// 				$show.animate({left: '-=' + (vWidth / 5) * 2}, "fast");
// 				page = i;
// 			}	else	{
// 				$show.animate({left: '+=' + vWidth / 5}, "slow");
// 				page--;
// 			}
// 		}
// 	});
// });



$(function(){

	// 院内消息图片滚动
	var isPlaying;
	var galleryPlaying = function($photo_box, $holder_box, $this, pictureWidth){
		var boxPosition = $photo_box.css("left");
		var $holderNum = $this.text();
		$photo_box.stop();
		if(!$photo_box.is(":animated")){
			if(($holderNum - 1) !== (boxPosition / pictureWidth)){
				$photo_box.animate({left: -($holderNum-1) * pictureWidth + "px"});
				$this.parents("ul#holder_ul").find("a").removeClass("current_pot");
				$this.addClass("current_pot");
				$description = $photo_box.find("img").eq($holderNum-1);
				$holder_box.find("p").replaceWith("<p>" + $description.attr("value") + "</p>");
			} else {return false;}
		}
	};
	var autoPlay = function(){
		var $holderBox = $("div#holder_box");
		var $photoBox = $(".photo_box");
		var pictureWidth = 230;
		var len = 5;
		if(isPlaying)clearTimeout(isPlaying);
		isPlaying=setTimeout(function(){
			if(!$photoBox.is(":animated")){
				var $currentIndex = $holderBox.find(".current_pot").text();
				$holderBox.find("ul#holder_ul").find("a").removeClass("current_pot");
				if($currentIndex == len){
					$photoBox.animate({left: "0px"});
					$holderBox.find("ul#holder_ul").find("a").eq(0).addClass("current_pot");
					$description = $photoBox.find("img").eq(0);
					$holderBox.find("p").replaceWith("<p>" + $description.attr("value") + "</p>");
				} else {
					$photoBox.animate({left: -($currentIndex) * pictureWidth + "px"});
					$holderBox.find("ul#holder_ul").find("a").eq($currentIndex).addClass("current_pot");
					$description = $photoBox.find("img").eq($currentIndex);
					$holderBox.find("p").replaceWith("<p>" + $description.attr("value") + "</p>");
				}
			}
			autoPlay();
		}, 5000);
	};
	autoPlay();

	// 绑定holder
	$(".holder").find("a").mouseover(function(){
		var $holder_box = $("div#holder_box");
		var $photo_box = $(".photo_box");
		var $this = $(this);
		var pictureWidth = 230;
		galleryPlaying($photo_box, $holder_box, $this, pictureWidth);
	});

	$("div#holder_box").mouseover(function(){
		if(isPlaying)clearTimeout(isPlaying);
	});
	$("div#holder_box").mouseout(function(){
		autoPlay();
	});
	


	// 专家介绍里面的动画
	var page = 1;
	var i = 3;
	var buttonAct = function($parent, direction){
		var $show = $parent.find("div.photoContList");
		var $picCont = $parent.find("div.photoContent");
		var vWidth = $picCont.width();
		if(	!$show.is(":animated")	){
			if(direction === "R"){
				if(	page == i	){
					$show.animate({left: '0px'}, "fast");
					page = 1;
				}	else	{
					$show.animate({left: '-=' + vWidth / 5}, "slow");
					page++;
				}
			} else {
				if(	 page == 1	){
					$show.animate({left: '-=' + (vWidth / 5) * 2}, "fast");
					page = i;
				}	else	{
					$show.animate({left: '+=' + vWidth / 5}, "slow");
					page--;
				}
			}
		}
	};
	$("span.RButton").click(function(){
		var $parent = $(this).parents("div#introducePhoto");
		buttonAct($parent, "R");
	});
	$("span.LButton").click(function(){
		var $parent = $(this).parents("div#introducePhoto");
		buttonAct($parent, "L");
	});

	// 菜单栏的切换 html & css样式有改动
	var $currentUl = $(".nav_menu").find(".current").parent("li").find("ul");
	var ensureLocation = function($this, left){
		switch(parseInt(left / 109)){
			case 0:
				break;
			case 1:
				if($this.find("li").length < 1){

				} else {
					$this.find("ul").css({
					left: -109 + "px"
					});
				}
				break;
			case 2:
				if($this.find("li").length < 1){

				} else {
					$this.find("ul").css({
					left: -109 + "px"
					});
				}
				break;
			case 3:
				if($this.find("li").length < 1){

				} else {
					$this.find("ul").css({
					left: -109 + "px"
					});
				}
				break;
			case 4:
				if($this.find("li").length < 4){
					$this.find("ul").css({
						left: 0 + "px"
					});
				} else {
					$this.find("ul").css({
						left: -($this.find("li").length - 4) * 109 + "px"
					});
				}
				break;
			case 5:
				if($this.find("li").length < 4){
					$this.find("ul").css({
						left: 0 + "px"
					});
				} else {
					$this.find("ul").css({
						left: -($this.find("li").length - 3) * 109 + "px"
					});
				}
				break;
			case 6:
				if($this.find("li").length < 3){
					$this.find("ul").css({
						left: 0 + "px"
					});
				} else {
					$this.find("ul").css({
						left: -($this.find("li").length - 2) * 109 + "px"
					});
				}
				break;
			case 7:
				if($this.find("li").length < 1){
					$this.find("ul").css({
						left: 0 + "px"
					});
				} else if($this.find("li").length < 9){
					$this.find("ul").css({
						left: -($this.find("li").length - 1) * 109 + "px"
					});
				}
				break;
		}
	};

	ensureLocation($currentUl.parent(), parseInt($currentUl.parent().position().left));
	$currentUl.css({
					display: "block", 
					width: $currentUl.width() * $currentUl.find("li").length + "px"
				});
	$(".nav_menu").find("li").mouseover(function(){
			$(".nav_menu").find("li").find("ul").css({display: "none"});
			var ulWidth = $(this).width() * $(this).find("li").length;
			
			ensureLocation($(this), parseInt($(this).position().left));
			$(this).find("ul").css({
									display: "block", 
									width: ulWidth + "px"
								});
	});
	$(".nav_menu").find("li").mouseout(function(){
		$(".nav_menu").find("li").find("ul").css({display: "none"});
		$currentUl.css({display: "block"});
	});

	// 专家介绍以及科室设置的切换
	var $departmentSettingButton = $(".introduce_setting .setting");
	var $expertIntroduceButton = $(".introduce_setting .introduce");
	$departmentSettingButton.click(function(){
		$(this).css({background: "#ffffff"});
		$(this).parent().find(".introduce").css({background: "#b3dff2"});
		$(this).parents(".introduce_setting").find("#introducePhoto").css({display: "none"});
		$(this).parents(".introduce_setting").find("#setting_content").css({display: "block"});
		return false;
	});
	$expertIntroduceButton.click(function(){
		$(this).css({background: "#ffffff"});
		$(this).parent().find(".setting").css({background: "#b3dff2"});
		$(this).parents(".introduce_setting").find("#introducePhoto").css({display: "block"});
		$(this).parents(".introduce_setting").find("#setting_content").css({display: "none"});
		return false;
	});

});