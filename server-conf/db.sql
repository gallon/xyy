SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema hospital
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` char(30) NOT NULL,
  `password` char(255) NOT NULL,
  `updated_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `session` VARCHAR(255) NULL,
  PRIMARY KEY (`id`))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE UNIQUE INDEX `username_UNIQUE` ON `user` (`username` ASC);

CREATE UNIQUE INDEX `session_UNIQUE` ON `user` (`session` ASC);


-- -----------------------------------------------------
-- Table `introduce`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `introduce`;

CREATE TABLE `introduce` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(30) NOT NULL,
  `content` mediumtext NOT NULL,
  `updated_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `article`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `article`;

CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(30) NOT NULL,
  `content` mediumtext NOT NULL,
  `category_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `updated_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(30) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `is_parent` int(11) NOT NULL DEFAULT '0',
  `updated_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `doctor`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `doctor`;

CREATE TABLE `doctor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` char(30) NOT NULL,
  `description` text NOT NULL,
  `phone` char(20) NOT NULL,
  `campus` char(30) NOT NULL,
  `portrait` text NULL,
  `position` char(50) NULL,
  `expert` int(11) DEFAULT '0',
  `updated_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `feedback`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(50) NOT NULL,
  `content` text NOT NULL,
  `fullname` char(30) NOT NULL,
  `email` char(60) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `comment` text NULL,
  `handler_id` int(11) NULL,
  `updated_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `link`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `link`;

CREATE TABLE `link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(30) NOT NULL,
  `url_link` char(256) NOT NULL,
  `updated_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Table `campus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `campus`;

CREATE TABLE `campus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(30) NOT NULL,
  `work_time` char(40) NOT NULL,
  `updated_at` TIMESTAMP NULL,
  `created_at` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- -----------------------------------------------------
-- Insert
-- -----------------------------------------------------
INSERT INTO `category` (
  `title`,
  `parent_id`,
  `is_parent`
) VALUES (
  "医院概况",
  0,
  1
);

INSERT INTO `category` (
  `title`,
  `parent_id`,
  `is_parent`
) VALUES (
  "医院新闻",
  0,
  1
);

INSERT INTO `category` (
  `title`,
  `parent_id`,
  `is_parent`
) VALUES (
  "健康保健",
  0,
  1
);

INSERT INTO `category` (
  `title`,
  `parent_id`,
  `is_parent`
) VALUES (
  "交流平台",
  0,
  1
);

INSERT INTO `category` (
  `title`,
  `parent_id`,
  `is_parent`
) VALUES (
  "就医指南",
  0,
  0
);

INSERT INTO `category` (
  `title`,
  `parent_id`,
  `is_parent`
) VALUES (
  "科室介绍",
  0,
  0
);

INSERT INTO `category` (
  `title`,
  `parent_id`,
  `is_parent`
) VALUES (
  "公费医疗",
  0,
  0
);

INSERT INTO `category` (
  `title`,
  `parent_id`,
  `is_parent`
) VALUES (
  "预防保健",
  3,
  0
);

INSERT INTO `category` (
  `title`,
  `parent_id`,
  `is_parent`
) VALUES (
  "健康教育",
  3,
  0
);
