<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Link model
 */
class Link_model extends CI_Model {

    /**
     * Table name
     *
     * @var string
     */
    private $table = 'link';

    public function __construct()
    {
        parent::__construct();
    }

    public function get_by_id($id)
    {
        return $this->db
                    ->select('*')
                    ->where('id', $id)
                    ->from($this->table)
                    ->get()
                    ->row();
    }
    public function get_all()
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->get()
                    ->result();
    }

    public function create($payload)
    {
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }
        $this->db->insert($this->table, $payload);
    }

    public function modify($id, $payload)
    {
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }
        $this->db
             ->set($payload)
             ->set('updated_at', 'current_timestamp', FALSE) // Update time
             ->where('id', $id)
             ->update($this->table);
    }

    public function delete($id)
    {
        $this->db
             ->where('id', $id)
             ->delete($this->table);
    }
}
