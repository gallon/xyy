<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Campus model
 */
class Campus_model extends CI_Model {

    /**
     * Table name
     *
     * @var string
     */
    private $table = 'campus';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get campus by campus's id
     *
     * @param int $id
     * @return object
     */
    public function get_by_id($id)
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->where('id', $id)
                    ->get()
                    ->row();
    }

    /**
     * Get all campuss
     *
     * @return array of object
     */
    public function get_all()
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->order_by('updated_at', 'desc')
                    ->get()
                    ->result();
    }

    public function create($payload)
    {
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }
        $this->db
             ->set($payload)
             ->set('updated_at', 'current_timestamp', FALSE) // Update time
             ->insert($this->table, $payload);
    }

    public function modify($id, $payload)
    {
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }
        $this->db
             ->set($payload)
             ->set('updated_at', 'current_timestamp', FALSE) // Update time
             ->where('id', $id)
             ->update($this->table);
    }

    public function delete($id)
    {
        $this->db
             ->where('id', $id)
             ->delete($this->table);
    }
}
