<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User model
 */
class User_model extends CI_Model {

    /**
     * Table
     *
     * @var string
     */
    private $table = 'user';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get all users
     *
     * @param string $username
     * @return object|null
     */
    public function get_all()
    {
        return $this->db->select('*')
                        ->from($this->table)
                        ->get()
                        ->result();
    }

    /**
     * Get user by id
     *
     * @param string $username
     * @return object|null
     */
    public function get_by_id($id)
    {
        $rv = $this->db->select('*')
                       ->from($this->table)
                       ->where('id', $id)
                       ->get()
                       ->row();
        if (count($rv) === 0)
        {
            return null;
        }

        return $rv;
    }

    /**
     * Get user by username
     *
     * @param string $username
     * @return object|null
     */
    public function get_by_username($username)
    {
        $rv = $this->db
                   ->select('*')
                   ->from($this->table)
                   ->where('username', $username)
                   ->get()
                   ->row();
        if (count($rv) === 0)
        {
            return null;
        }

        return $rv;
    }

    /**
     * Get a user by session
     *
     * @param string $session
     * @return object|null
     */
    public function get_by_session($session)
    {
        $rv = $this->db
                   ->select('*')
                   ->from($this->table)
                   ->where('session', $session)
                   ->get()
                   ->row();

        if (count($rv) === 0)
        {
            return null;
        }

        return $rv;
    }

    /**
     * Create a new user
     *
     * @param array $payload
     * @return object|null
     * @todo 过滤输入
     */
    public function create($payload)
    {
        // 确保不会插入 id
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }

        if ($this->db->insert($this->table, $payload))
        {
            return $this->get_by_id($this->db->insert_id());
        }
    }

    /**
     * Create login session
     *
     * @param object $user
     * @return string
     */
    public function create_login_session($user)
    {
        // 应该不会碰撞的吧啦啦啦啦
        $session = hash_str(time() . $user->id);

        // 更新数据库记录
        $this->db
             ->where('id', $user->id)
             ->update($this->table, array(
            'session' => $session
             ));

        return $session;
    }

    /**
     * Destroy login session
     *
     * @param object $user
     * @return void
     */
    public function destory_login_session($user)
    {
        $this->db
             ->where('id', $user->id)
             ->update($this->table, array(
            'session' => null
             ));
    }

    /**
     * Encrypt_password
     *
     * @param string $username
     * @param string $password
     * @return string
     */
    public function encrypt_password($username, $password)
    {
        // 当前使用的加密公式为 hash(password)
        return hash_str($username . $password);
    }

    /**
     * Check password
     *
     * @param object $user
     * @param string $password
     * @return bool
     */
    public function check_password($user, $password)
    {
        // TODO 想要更好的安全性？考虑一下计时攻击。
        return $this->encrypt_password($user->username, $password) === $user->password;
    }

    /**
     * Check username
     *
     * @param string $username
     * @return bool
     */
    public function check_username($username)
    {
        $rv = $this->db
                   ->select('*')
                   ->from($this->table)
                   ->where('username', $username)
                   ->get()
                   ->row();
        if (count($rv) === 0)
        {
            return true;
        }
        return false;
    }


    /**
     * Modify user detail
     *
     * @param object $user
     * @param array $new
     * @return void
     */
    public function modify($user, $payload)
    {
        // 确保不会插入 id and session
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }
        if (isset($payload['session']))
        {
            unset($payload['session']);
        }
        $this->db
             ->set($payload)
             ->set('updated_at', 'current_timestamp', FALSE) // Update time
             ->where('id', $user->id)
             ->update($this->table);
    }

    public function get_count()
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->get()
                    ->num_rows();
    }
}
