<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Default model
 */
class Default_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();

        $this->load->database();
        $this->db->query('set time_zone="+8:00"');
    }
}
