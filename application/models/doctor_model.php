<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Doctor model
 */
class Doctor_model extends CI_Model {

    /**
     * Table
     *
     * @var string
     */
    private $table = 'doctor';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get all doctors
     *
     * @param string $doctorname
     * @return object|null
     */
    public function get_all()
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->get()
                    ->result();
    }

    public function get_expert()
    {
        return $this->db
                    ->select('*')
                    ->where('expert', 1)
                    ->from($this->table)
                    ->get()
                    ->result();
    }

    /**
     * Get doctor by id
     *
     * @param string $doctorname
     * @return object|null
     */
    public function get_by_id($id)
    {
        $rv = $this->db
                   ->select('*')
                   ->from($this->table)
                   ->where('id', $id)
                   ->get()
                   ->row();
        if (count($rv) === 0)
        {
            return null;
        }

        return $rv;
    }

    /**
     * Create a new doctor
     *
     * @param array $payload
     * @return object|null
     * @todo 过滤输入
     */
    public function create($payload)
    {
        // 确保不会插入 id
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }

        if ($this->db->insert($this->table, $payload))
        {
            return $this->get_by_id($this->db->insert_id());
        }
    }

    /**
     * Modify doctor detail
     *
     * @param object $doctor
     * @param array $new
     * @return void
     */
    public function modify($doctor, $payload)
    {
        // 确保不会插入 id and session
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }
        $this->db
             ->set($payload)
             ->set('updated_at', 'current_timestamp', FALSE) // Update time
             ->where('id', $doctor->id)
             ->update($this->table);
    }

    public function delete($doctor)
    {
        $this->db
             ->where('id', $doctor->id)
             ->delete($this->table);
    }
}
