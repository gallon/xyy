<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Article model
 */
class Article_model extends CI_Model {

    /**
     * Table name
     *
     * @var string
     */
    private $table = 'article';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get article by article's id
     *
     * @param int $id
     * @return object
     */
    public function get_by_id($id)
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->where('id', $id)
                    ->get()
                    ->row();
    }

    /**
     * Get articles by categor's id
     *
     * @param int $category_id
     * @return array of object
     */
    public function get_by_category($category_id)
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->where('category_id', $category_id)
                    ->order_by('updated_at', 'desc')
                    ->get()
                    ->result();
    }

    /**
     * Get all articles
     *
     * @return array of object
     */
    public function get_all()
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->order_by('updated_at', 'desc')
                    ->get()
                    ->result();
    }

    public function create($payload)
    {
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }
        $this->db
             ->set($payload)
             ->set('updated_at', 'current_timestamp', FALSE) // Update time
             ->insert($this->table, $payload);
    }

    public function modify($id, $payload)
    {
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }
        $this->db
             ->set($payload)
             ->set('updated_at', 'current_timestamp', FALSE) // Update time
             ->where('id', $id)
             ->update($this->table);
    }

    public function delete($id)
    {
        $this->db
             ->where('id', $id)
             ->delete($this->table);
    }

    public function get_count()
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->get()
                    ->num_rows();
    }
}
