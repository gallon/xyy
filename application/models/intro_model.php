<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Introduce model
 */
class Intro_model extends CI_Model {

    /**
     * Table name
     *
     * @var string
     */
    private $table = 'introduce';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get introduce by id
     *
     * @return object|null
     */
    public function get_by_id($id)
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->where('id', $id)
                    ->get()
                    ->row();
    }

    /**
     * Get all introduces
     *
     * @return a object|array
     */
    public function get_one()
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->get()
                    ->row();

    }

    /**
     * Modify an old introduce
     *
     * @param array $payload
     * @return void
     */
    public function modify($payload)
    {
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }
        $id = $this->get_one()->id;
        if ($id)
        {
            $this->db
                 ->set($payload)
                 ->set('updated_at', 'current_timestamp', FALSE) // Update time
                 ->where('id', $id)
                 ->update($this->table);
        }
        else
        {
            $this->db
                 ->set($payload)
                 ->insert($this->table);
        }
    }
}
