<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Category model
 */
class Category_model extends CI_Model {

    /**
     * Table name
     *
     * @var string
     */
    private $table = 'category';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get category by id
     *
     * @return object|null
     */
    public function get_by_id($id)
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->where('id', $id)
                    ->get()
                    ->row();
    }

    /**
     * Get categories which are parent category
     *
     * @return array of object
     */
    public function get_parent()
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->where('parent_id', 0)
                    ->get()
                    ->result();
    }

    /**
     * Get categories which are child category by parent id
     *
     * @param int $id
     * @return array of object
     */
    public function get_by_parent($id)
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->where('parent_id', $id)
                    ->get()
                    ->result();
    }

    /**
     * Get all categories
     *
     * @return array of object
     */
    public function get_all()
    {
        return $this->db
                    ->select('*')
                    ->from($this->table)
                    ->get()
                    ->result();
    }

    /**
     * Create a new category
     *
     * @param array $payload
     * @return void
     */
    public function create($payload)
    {
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }
        $this->db->insert($this->table, $payload);
    }

    /**
     * Modify an old category
     *
     * @param array $payload
     * @return void
     */
    public function modify($id, $payload)
    {
        if (isset($payload['id']))
        {
            unset($payload['id']);
        }
        $this->db->update($this->table, $payload, array('id' => $id));
    }

    /**
     * Delete a category
     *
     * @param array $payload
     * @return void
     */
    public function delete($id)
    {
        $this->db->delete($this->table, array('id' => $id));
    }
}
