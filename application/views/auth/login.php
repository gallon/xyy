<!DOCTYPE html>
<html>
  <head>
    <title>后台管理系统登陆</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.css'); ?>" />
    <meta charset="utf-8" />
  </head>
  <body class="well">
    <h1 class="text-center text-info">后台管理系统</h1>
    <div class="row">
      <div class="col-md-2 col-md-offset-5">
        <?php if (isset($alert_error)): ?>
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>错误!</strong><?php echo $alert_error;?>
          </div>
        <?php endif; ?>
        <form action="<?php echo base_url("user/login"); ?>" method="POST" role="form">
          <div class="form-group">
            <label>用户名</label>
            <input type="text" class="form-control" name= "username" placeholder="Username">
          </div>
          <div class="form-group">
            <label>密码</label>
            <input type="password" class="form-control" name="password" placeholder="Password">
          </div>
          <button type="submit" class="btn btn-lg btn-primary btn-block">登陆</button>
        </form>
      </div>
    </div>
  </body>
  <script type="text/javascript" src="<?php echo base_url('static/js/jquery.js'); ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('static/js/bootstrap.js'); ?>"></script>
</html>
