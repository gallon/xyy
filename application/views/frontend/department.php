    <!-- content -->
	<div class="content">
	  <div class="title">
		<h1 class="Chtitle">科室介绍</h1>
		<h4 class="Entitle">Department</h4>
	  </div>
	  <div class="wrap">
		<div class="part">
		  <ul>
			<a class="department part_cur" href="#"><li>科室介绍</li></a>
		  </ul>
		</div>
		<div class="department_intro">
		  <div class="cur">当前位置：<a href="#">首页</a> > <a href="#">科室介绍</a></div>
		  <h2>科室介绍</h2>
          <div class="list">
            <?php foreach ($articles as $rv): ?>
              <?php
               $article = $rv['article'];
              ?>
			  <div class="intro">
			    <h4><a href="<?php echo base_url('article/' . $article->id); ?>"><?php echo $article->title; ?></a></h4>
                <p>
                  <?php echo substr(strip_tags($article->content), 0, 25); ?>
                </p>
			  </div>
            <?php endforeach; ?>
            <hr/>
			<div class="pagination" style="margin-left: auto; margin-right: auto; width:350px;">
            </div>
          </div>
		</div>
	  </div>
	</div>
    
    <script>
      $(function() {
          $('.pagination').pagination({
              items: <?php echo $all_count; ?>,
              /* TODO Article count */
              itemsOnPage: 10,
              prevText: '前一页',
              nextText: '后一页',
              currentPage: <?php echo $page; ?>,
              cssStyle: 'compact-theme',
              onPageClick: function (pageNumber, event) {
                  location.href = "/department/" + pageNumber;
              }
          });
      });
    </script>
