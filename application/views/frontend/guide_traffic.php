    <!-- content -->
	<div class="content">
	  <div class="title">
		<h1 class="Chtitle">就医指南</h1>
		<h4 class="Entitle">Medical Guide</h4>
	  </div>
	  <div class="wrap">
		<div class="part">
		  <ul>
            <?php if (count($guides) > 0): ?>
              <?php foreach ($guides as $guide): ?>
				<a class="hospital_news part_other" href="<?php echo base_url('guide/' . $guide['article']->id); ?>"><li><?php echo $guide['article']->title; ?></li></a>
              <?php endforeach; ?>
            <?php endif; ?>
			<a class="traffic part_other" href="/guide/traffic"><li>交通指南</li></a>
		  </ul>
		</div>
		<div class="medical_guide">
		  <div class="cur">当前位置：<a href="#">首页</a> > <a href="#">就医指南</a> > <a href="#">交通指南</a></div>
		  <h2>交通指南</h2>
		  <div class="guide">
			<div class="map">
			  <iframe src="<?php echo base_url('static/frontend/testMap.html'); ?>" width="712" height="460"></iframe>
			</div>
		  </div>
		</div>
	  </div>
	</div>
