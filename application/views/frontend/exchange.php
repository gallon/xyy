    <!-- content -->
	<div class="content">
	  <div class="title">
		<h1 class="Chtitle">交流平台</h1>
		<h4 class="Entitle">Communication Platform</h4>
	  </div>
	  <div class="wrap">
		<div class="part">
          <ul>
			<a class="feedback part_other" href="/feedback"><li>意见反馈</li></a>
            <?php if (count($exchange_cates) > 0): ?>
              <?php foreach ($exchange_cates as $exchange): ?>
				<a class="download part_other" href="<?php echo base_url('exchange/' . $exchange['category']->id); ?>"><li><?php echo $exchange['category']->title; ?></li></a>
              <?php endforeach; ?>
            <?php endif; ?>
		  </ul>
		</div>
		<div class="communication">
		  <div class="cur">当前位置：<a href="#">首页</a> > <a href="#">交流平台</a> > <a href="#"><?php echo $page_title; ?></a></div>
		  <h2><?php echo $page_title; ?></h2>
		  <div class="list">
            <?php foreach ($articles as $rv): ?>
              <?php
               $article = $rv['article'];
              ?>
			  <div class="intro">
			    <h4><a href="<?php echo base_url('article/' . $article->id); ?>"><?php echo $article->title; ?></a><span class="date"><?php echo $article->updated_at; ?></span></h4>
                <p>
                  <?php echo substr(strip_tags($article->content), 0, 25); ?>
                </p>
			  </div>
            <?php endforeach; ?>
            <hr/>
			<div class="pagination" style="margin-left: auto; margin-right: auto; width:350px;">
            </div>            
		  </div>
		</div>
	  </div>
	</div>
    <script>
      $(function() {
          $('.pagination').pagination({
              items: <?php echo $all_count; ?>,
              /* TODO Article count */
              itemsOnPage: 10,
              prevText: '前一页',
              nextText: '后一页',
              currentPage: <?php echo $page; ?>,
              cssStyle: 'compact-theme',
              onPageClick: function (pageNumber, event) {
                  location.href = "/exchange/<?php echo $page_id?>/" + pageNumber;
              }
          });
      });
    </script>
