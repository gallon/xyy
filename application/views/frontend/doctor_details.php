<!-- content -->
		<div class="content">
		  <div class="title">
			<h1 class="Chtitle">医生介绍</h1>
			<h4 class="Entitle">Doctor</h4>	
		  </div>
		  <div class="wrap">
			<div class="part">
              <ul>
				<a class="hospital part_other" href="<?php echo base_url('intro/hospital'); ?>"><li>医院介绍</li></a>
				<a class="expert part_other" href="<?php echo base_url('intro/expert'); ?>"><li>专家介绍</li></a>
				<a class="doctor part_other" href="<?php echo base_url('intro/doctor'); ?>"><li>医护人员</li></a>
                <?php if (count($intro_cates) > 0): ?>
                  <?php foreach ($intro_cates as $intro): ?>
					<a class="rules part_other" href="<?php echo base_url('intro/' . $intro['category']->id); ?>"><li><?php echo $intro['category']->title; ?></li></a>
                  <?php endforeach; ?>
                <?php endif; ?>
			  </ul>
			</div>
			<div class="introduction">
			  <div class="cur">当前位置：<a href="#">首页</a> > <a href="#">医院介绍</a> > <a href="#">医生介绍</a></div>
			  <h2>医生介绍</h2>
			  <div class="essay">
                <img src="<?php echo $doctor->portrait; ?>" style="width:250px; height:250px; float:none;">
                <p style="font-size:18px; padding-top:32px;">
                  <strong>姓名:</strong>
                  <?php echo $doctor->fullname; ?>
                </p>
                <hr/>
                <p style="font-size:18px; padding-top:32px;">
                  <strong>职位:</strong>
                  <?php echo $doctor->position; ?>
                </p>
                <hr/>
                <p style="font-size:18px; padding-top:32px;">
                  <strong>联系方式:</strong>
                  <?php echo $doctor->phone; ?>
                </p>
                <hr/>
                <p style="font-size:18px; padding-top:32px;">
                  <strong>描述:</strong>
                  <?php echo $doctor->description; ?>
                </p>
			  </div>
			</div>
		  </div>	
		</div>
