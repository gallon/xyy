<!-- content -->
		<div class="content">
		  <div class="title">
			<h1 class="Chtitle">文章</h1>
			<h4 class="Entitle">Article</h4>	
		  </div>
		  <div class="wrap">
			<div class="part">
              <ul>
				<a class="hospital part_other" href="<?php echo base_url($this_page['url']); ?>"><li><?php echo $this_page['title']; ?></li></a>
			  </ul>
			</div>
			<div class="introduction">
			  <div class="cur">当前位置：<a href="#">首页</a> > <a href="#"><?php echo $this_page['parent']; ?></a> > <a href="#"><?php echo $this_page['title']; ?></a></div>
              <h2><?php echo $this_page['title']; ?></h2>
			  <div class="essay">
			    <h3 class="essay_title"><?php echo $article->title; ?></h2>
                <div class="data">广工校医院 <?php echo $article->updated_at; ?></div>
                <p><?php echo $article->content; ?></p>
			  </div>
			</div>
		  </div>
		</div>
