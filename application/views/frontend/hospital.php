<!-- content -->
		<div class="content">
		  <div class="title">
			<h1 class="Chtitle">医院介绍</h1>
			<h4 class="Entitle">Introduction</h4>	
		  </div>
		  <div class="wrap">
			<div class="part">
              <ul>
				<a class="hospital part_other" href="<?php echo base_url('intro/hospital'); ?>"><li>医院介绍</li></a>
				<a class="expert part_other" href="<?php echo base_url('intro/expert'); ?>"><li>专家介绍</li></a>
				<a class="doctor part_other" href="<?php echo base_url('intro/doctor'); ?>"><li>医护人员</li></a>
                <?php if (count($intro_cates) > 0): ?>
                  <?php foreach ($intro_cates as $intro): ?>
					<a class="rules part_other" href="<?php echo base_url('intro/' . $intro['category']->title); ?>"><li><?php echo $intro['category']->title; ?></li></a>
                  <?php endforeach; ?>
                <?php endif; ?>
			  </ul>
			</div>
			<div class="introduction">
			  <div class="cur">当前位置：<a href="#">首页</a> > <a href="#">医院介绍</a> > <a href="#">医院简介</a></div>
			  <h2>医院简介</h2>
			  <div class="essay">
                <?php echo $introduce_article->content; ?>
			  </div>
			</div>
		  </div>	
		</div>
