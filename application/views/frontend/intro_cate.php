    <!-- content -->
	<div class="content">
	  <div class="title">
		<h1 class="Chtitle">医院介绍</h1>
		<h4 class="Entitle">Introduction</h4>
	  </div>
	  <div class="wrap">
		<div class="part">
          <ul>
			<a class="hospital part_other" href="<?php echo base_url('intro/hospital'); ?>"><li>医院介绍</li></a>
			<a class="expert part_other" href="<?php echo base_url('intro/expert'); ?>"><li>专家介绍</li></a>
			<a class="doctor part_other" href="<?php echo base_url('intro/doctor'); ?>"><li>医护人员</li></a>
            <?php if (count($intro_cates) > 0): ?>
              <?php foreach ($intro_cates as $intro): ?>
				<a class="rules part_other" href="<?php echo base_url('intro/' . $intro['category']->id); ?>"><li><?php echo $intro['category']->title; ?></li></a>
              <?php endforeach; ?>
            <?php endif; ?>
		  </ul>
		</div>
		<div class="introduction">
		  <div class="cur">当前位置：<a href="#">首页</a> > <a href="#">医院介绍</a> > <a href="#"><?php echo $page_title; ?></a></div>
		  <h2><?php echo $page_title; ?></h2>
		  <div class="list">
            <?php foreach ($articles as $rv): ?>
              <?php
               $article = $rv['article'];
              ?>
			  <div class="intro">
			    <h4><a href="<?php echo base_url('article/' . $article->id); ?>"><?php echo $article->title; ?></a></h4>
                <p>
                  <?php echo substr(strip_tags($article->content), 0, 25); ?>
                </p>
			  </div>
            <?php endforeach; ?>
            <hr/>
            <div class="pagination" style="margin-left: auto; margin-right: auto; width:350px;">
            </div>
		  </div>
		</div>
	  </div>
	</div>
    <script>
      $(function() {
          $('.pagination').pagination({
              items: 20,
              /* TODO */
              itemsOnPage: 5,
              currentPage: <?php echo $page; ?>,
              cssStyle: 'compact-theme',
              onPageClick: function (pageNumber, event) {
                  location.href = "/intro/<?php echo $page_id; ?>/" + pageNumber;
              }
          });
      });
    </script>
