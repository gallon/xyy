<!-- content -->
<div class="content">
  <!-- Message of hosipital -->
  <div class="nav_block message">
	<div class="titlebg">
	  <h2>院内消息<span class="more"><a href="<?php echo base_url('news'); ?>">更多&gt&gt</a></span></h2>
	</div>

	<div class="gallery">
	  <div class="photo_box">
		<a href=""><img src="<?php echo base_url('static/frontend/images/p1.jpg'); ?>" class="current" value="HaHaHa"></a>
		<a href=""><img src="<?php echo base_url('static/frontend/images/p2.jpg'); ?>" value="OhOhOh"></a>
		<a href=""><img src="<?php echo base_url('static/frontend/images/p3.jpg'); ?>" value="BuBuBu"></a>
		<a href=""><img src="<?php echo base_url('static/frontend/images/p4.jpg'); ?>" value="NoNoNo"></a>
		<a href=""><img src="<?php echo base_url('static/frontend/images/p5.jpg'); ?>" value="AhAhAh"></a>
	  </div>
	  <div class="holder" id="holder_box">
		<ul id="holder_ul">
		  <li><a href='#' class="current_pot">1</a></li>
		  <li><a href='#'>2</a></li>
		  <li><a href='#'>3</a></li>
		  <li><a href='#'>4</a></li>
		  <li><a href='#'>5</a></li>
		</ul>
		<p>高校防疫</p>
	  </div>
	</div>
    <?php
    $count = 0;
    ?>
    <?php if (count($articles['news']) > 0): ?>
	  <ul>
        <?php
        $news_article = $articles['news'][0];
        ?>
        <?php foreach ($news_article['article'] as $article): ?>
          <?php
           if ($count == 8)
          {
              break;
          }
          $count ++;
          ?>
	      <li>
	        <a href="<?php echo base_url('article/' . $article->id); ?>"><?php echo $article->title; ?></a>
	        <span class="time">[<?php echo $article->updated_at; ?>]</span>
	      </li>
        <?php endforeach; ?>
	  </ul>
    <?php endif; ?>
  </div>

  <!-- medical services at state expense -->
  <div class="nav_block medical_service">
	<div class="titlebg">
	  <h2>公费医疗<span class="more"><a href="<?php echo base_url('/social'); ?>">更多&gt&gt</a></span></h2>
	</div>
    <ul>
      <?php
      $count = 0;
      $social_articles = $articles['social'];
      ?>
      <?php foreach ($social_articles as $rv): ?>
        <?php
         $article = $rv['article'];
        if ($count >= 8)
        {
            break;
        }
        $count ++;
        ?>
	    <li><a href="<?php echo base_url('article/' . $article->id); ?>"><?php echo $article->title; ?></a>	</li>
      <?php endforeach; ?>
    </ul>
  </div>

  <!-- Preventive health care -->
  <div class="nav_block health_care_list">
	<div class="titlebg">
	  <h2>预防保健<span class="more"><a href="<?php echo base_url('/health/8'); ?>">更多&gt&gt</a></span></h2>
	</div>
	<ul>
      <?php
      $count = 0;
      $health_1_articles = $articles['health_1'];
      ?>
      <?php foreach ($health_1_articles as $rv): ?>
        <?php
         $article = $rv['article'];
        if ($count >= 5)
        {
            break;
        }
        $count ++;
        ?>
	    <li><a href="<?php echo base_url('article/' . $article->id); ?>"><?php echo $article->title; ?></a>	<span class="time">[<?php echo $article->updated_at; ?>]</span></li>
      <?php endforeach; ?>
	</ul>
  </div>
  <!--  -->

  <!-- Health education -->
  <div class="nav_block health_educate">
	<div class="titlebg">
	  <h2>健康教育<span class="more"><a href="<?php echo base_url('/health/9'); ?>">更多&gt&gt</a></span></h2>
	</div>
	<ul>
      <?php
      $count = 0;
      $health_2_articles = $articles['health_2'];
      ?>
      <?php foreach ($health_2_articles as $rv): ?>
        <?php
         $article = $rv['article'];
        if ($count >= 5)
        {
            break;
        }
        $count ++;
        ?>
	    <li><a href="<?php echo base_url('article/' . $article->id); ?>"><?php echo $article->title; ?></a></li>
      <?php endforeach; ?>
	</ul>
  </div>

  <!--  -->

  <!-- Normal Experts Schedule -->
  <div class="nav_block exper_schedule">
	<div class="titlebg">
	  <h2>常规出诊时间</h2>
	</div>
	<ul>
      <?php
      $count = 0;
      ?>
      <?php foreach ($campuses as $campus): ?>
        <?php
         if ($count >= 5)
        {
            break;
        }
        $count ++;
        ?>
	    <li>
          <a href="#"><?php echo $campus->title; ?>:<span><?php echo $campus->work_time; ?></span></a>
 	    </li>
      <?php endforeach; ?>
	</ul>
  </div>
  <!--  -->

  <!--Expert Introduce and Departments Setting -->
  <div class="nav_block introduce_setting">
	<div class="titlebg">
	  <h2>
        <p class="introduce"><a href="#">专家介绍</a></p>
		<p class="setting"><a href="<?php echo base_url('department'); ?>">科室设置</a></p>
	  </h2>
	</div>
	<div id="introducePhoto">
	  <div class="changeButt">
		<span class="LButton"></span>
		<span class="RButton"></span>
	  </div>

	  <div class="photoContent">
		<div  class="photoContList">
		  <ul>
            <?php
            $count = 0;
            shuffle($experts);
            ?>
            <?php foreach ($experts as $expert): ?>
              <?php
               if ($count >= 5)
              {
                  break;
              }
              $count ++;
              ?>
			  <li><a href="<?php echo base_url('intro/doctor/' . $expert->id); ?>"><img src="<?php echo $expert->portrait; ?>" alt="" /></a></li>
            <?php endforeach; ?>
		  </ul>
		</div>
	  </div>
	</div>
	<ul id="setting_content">
      <?php
      $count = 0;
      $dep_articles = $articles['department'];
      ?>
      <?php foreach ($dep_articles as $rv): ?>
        <?php
         $article = $rv['article'];
        if ($count >= 6)
        {
            break;
        }
        $count ++;
        ?>
	    <li><a href="<?php echo base_url('article/' . $article->id); ?>"><?php echo $article->title; ?></a></li>
      <?php endforeach; ?>
	</ul>
  </div>
  <!--  -->

  <!-- Doctor's Phone Number -->
  <div class="nav_block phone_number">
	<div class="titlebg">
	  <h2>各校医电话<span class="more"><a href="<?php echo base_url('intro/doctor'); ?>">更多&gt&gt</a></span></h2>
	</div>
	<ul>
      <?php
      $count = 0;
      shuffle($doctors);
      ?>
      <?php foreach ($doctors as $doctor): ?>
        <?php
         if ($count >= 6)
        {
            break;
        }
        $count ++;
        ?>
	    <li>
          <div>
            <span><?php echo $doctor->phone; ?></span>
            <span><?php echo $doctor->fullname; ?></span>
            <span><?php echo $doctor->campus; ?></span>
          </div>
	    </li>
      <?php endforeach; ?>
	</ul>
  </div>
  <!--  -->

  <!--  Feedback -->
  <div class="nav_block feed_back">
	<div class="titlebg">
	  <h2>意见反馈<span class="more"><a href="<?php echo base_url('/feedback'); ?>">更多&gt&gt</a></span></h2>
	</div>
	<ul>
      <?php
      $good_feedback = $feedbacks['good'];
      $bad_feedback = $feedbacks['bad'];
      $count = 0;
      ?>
      <?php foreach ($good_feedback as $rv): ?>
        <?php
         $feedback = $rv['feedback'];
        if ($count >= 6)
        {
            break;
        }
        $count ++;
        ?>
	    <li><a href="<?php echo base_url('feedback/details/' . $feedback->id); ?>"><?php echo $feedback->title; ?></a>	<span class="replay">[已回复]</span></li>
      <?php endforeach; ?>
      <?php foreach ($bad_feedback as $rv): ?>
        <?php
         $feedback = $rv['feedback'];
        if ($count >= 6)
        {
            break;
        }
        $count ++;
        ?>
	    <li><a href="<?php echo base_url('feedback/details/' . $feedback->id); ?>"><?php echo $feedback->title; ?></a>	<span class="replay">[未回复]</span></li>
      <?php endforeach; ?>
	</ul>
  </div>
  <!--  -->

</div>


<!-- / content -->
