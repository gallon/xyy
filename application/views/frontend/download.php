    <!-- content -->
	<div class="content">
	  <div class="title">
		<h1 class="Chtitle">交流平台</h1>
		<h4 class="Entitle">Communication Platform</h4>
	  </div>
	  <div class="wrap">
		<div class="part">
		  <ul>
			<a class="feedback part_other" href="feedback.html"><li>意见反馈</li></a>
			<a class="download part_cur" href="download.html"><li>下载专区</li></a>
		  </ul>
		</div>
		<div class="communication">
		  <div class="cur">当前位置：<a href="#">首页</a> > <a href="#">交流平台</a> > <a href="#">下载专区</a></div>
		  <h2>下载专区</h2>
		  <div class="list">
			<div class="intro">
			  <img src="../images/news.jpg">
			  <h4><a href="#">公费医疗特殊诊治申请表</a><span class="date">[02-28]</span></h4>
			</div>
			<div class="intro">
			  <img src="../images/news.jpg">
			  <h4><a href="#">教工异地就医申请表 2014 </a><span class="date">[02-28]</span></h4>
			</div>
			<div class="intro">
			  <img src="../images/news.jpg">
			  <h4><a href="#">关于申请恢复入学资格/复学的体检通知</a><span class="date">[02-26]</span></h4>
			</div>
			<div class="intro">
			  <img src="../images/news.jpg">
			  <h4><a href="#">广东工业大学广州市内非指定医院就医申请</a><span class="date">[02-25]</span></h4>
			</div>
			<div class="intro">
			  <img src="../images/news.jpg">
			  <h4><a href="#">转诊制度及选定医疗机构</a><span class="date">2013-11-28</span></h4>
			</div>
			<div class="data">
						共&nbsp;60&nbsp;条&nbsp;&nbsp;&nbsp;1&nbsp;/&nbsp;20&nbsp;页&nbsp;&nbsp;&nbsp;
			  <a href="#">首页</a>&nbsp;&nbsp;&nbsp;
			  <a href="#">上一页</a>&nbsp;&nbsp;&nbsp;
						第&nbsp;<span>1</span>&nbsp;页&nbsp;&nbsp;&nbsp;
			  <a href="#">下一页</a>&nbsp;&nbsp;&nbsp;
			  <a href="#">尾页</a>
			</div>
		  </div>
		</div>
	  </div>
	</div>
