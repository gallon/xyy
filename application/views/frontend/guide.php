<!-- content -->
		<div class="content">
		  <div class="title">
			<h1 class="Chtitle">就医指南</h1>
			<h4 class="Entitle">Medical Guide</h4>	
		  </div>
		  <div class="wrap">
			<div class="part">
              <ul>
                <?php if (count($guides) > 0): ?>
                  <?php foreach ($guides as $guide): ?>
				    <a class="ordinary part_other" href="<?php echo base_url('guide/' . $guide['article']->id); ?>"><li><?php echo $guide['article']->title; ?></li></a>
                  <?php endforeach; ?>
                <?php endif; ?>
			    <a class="traffic part_other" href="/guide/traffic"><li>交通指南</li></a>
		      </ul>
			</div>
			<div class="medical_guide">
			  <div class="cur">当前位置：<a href="#">首页</a> > <a href="#">就医指南</a> > <a href="#"><?php echo $article->title; ?></a></div>
			  <h2><?php echo $article->title; ?></h2>
			  <div class="essay">
                <?php echo $article->content; ?>
			  </div>
			</div>
		  </div>	
		</div>
