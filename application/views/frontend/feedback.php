    <!-- content -->
	<div class="content">
	  <div class="title">
		<h1 class="Chtitle">交流平台</h1>
		<h4 class="Entitle">Communication Platform</h4>
	  </div>
	  <div class="wrap">
		<div class="clear">
		  <div class="part">
			<ul>
			  <a class="feedback part_other" href="/feedback"><li>意见反馈</li></a>
              <?php if (count($exchange_cates) > 0): ?>
                <?php foreach ($exchange_cates as $exchange): ?>
				  <a class="download part_other" href="<?php echo base_url('exchange/' . $exchange['category']->id); ?>"><li><?php echo $exchange['category']->title; ?></li></a>
                <?php endforeach; ?>
              <?php endif; ?>
			</ul>
		  </div>
		  <div class="communication">
			<div class="block">
			  <div class="cur">当前位置：<a href="#">首页</a> > <a href="#">交流平台</a> > <a href="#">意见反馈</a></div>
			  <h2>意见反馈</h2>
			  <div class="list">
                <?php foreach ($feedbacks as $rv): ?>
                  <?php
                   $feedback = $rv['feedback'];
                  $handler = $rv['handler'];
                  ?>

				  <div class="intro">
				    <h4><a href="<?php echo base_url('feedback/details/' . $feedback->id); ?>"><?php echo $feedback->title; ?></a><span class="status">[<?php if ($feedback->status == 0) { echo '未'; } else { echo '已'; } ?>回复]</span></h4>
				    <p>[姓名]：<?php echo $feedback->fullname; ?></p>
				    <p>[时间]：<?php echo $feedback->created_at; ?></p>
				    <p>[内容]：<?php echo $feedback->content; ?></p>
				  </div>
                <?php endforeach; ?>
                <hr/>
				<div class="pagination" style="margin-left: auto; margin-right: auto; width:350px;">
                </div>
			  </div>
			</div>
            <div class="commit">
			  <form role="form" action="/feedback/add" method="post">
			    <div>
				  <span class="need">*</span><span class="kind">姓名 : </span><input class="name" name="fullname">
			    </div>
			    <div>
				  <span class="need">*</span><span class="kind">邮箱 : </span><input class="email" name="email">
			    </div>
			    <div>
				  <span class="need">*</span><span class="kind">主题 : </span><input class="theme" name="title">
			    </div>
			    <div>
				  <span class="need">*</span><span class="kind">内容 : </span><textarea name="content"></textarea>
			    </div>
			    <button type="submit" class="button">意见提交</button>
			  </form>
		    </div>
		  </div>
		</div>
	  </div>
	</div>
    
    <script>
      $(function() {
          $('.pagination').pagination({
              items: <?php echo $all_count; ?>,
              /* TODO Article count */
              itemsOnPage: 5,
              prevText: '前一页',
              nextText: '后一页',
              currentPage: <?php echo $page; ?>,
              cssStyle: 'compact-theme',
              onPageClick: function (pageNumber, event) {
                  location.href = "/feedback/" + pageNumber;
              }
          });
      });
    </script>
