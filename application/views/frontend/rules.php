    <!-- content -->
	<div class="content">
	  <div class="title">
		<h1 class="Chtitle">医院介绍</h1>
		<h4 class="Entitle">Introduction</h4>
	  </div>
	  <div class="wrap">
		<div class="part">
		  <ul>
			<a class="hospital part_other" href="hospital_intro.html"><li>医院介绍</li></a>
			<a class="expert part_other" href="expert_intro.html"><li>专家介绍</li></a>
			<a class="doctor part_other" href="doctor_intro.html"><li>医护人员</li></a>
			<a class="rules part_cur" href="rules.html"><li>规章制度</li></a>
		  </ul>
		</div>
		<div class="introduction">
		  <div class="cur">当前位置：<a href="#">首页</a> > <a href="#">医院介绍</a> > <a href="#">规章制度</a></div>
		  <h2>规章制度</h2>
		  <div class="list">
			<div class="intro">
			  <h4><a href="#">教职工公费医疗管理规定</a></h4>
			  <p>教工公费医疗管理制度 根据国家、省有关文件规定，实行干部职工个人自负一定比例的医药费，同时发放医疗补贴的办法。一、 报销比例 (一)在职教工：校内门诊报销85%；个人自负15%；校外门诊报销80%，个人自负20%；校内住院报销94%，个人自负6%；校外住院报销90%，个人自负10%……</p>
			</div>
			<div class="intro">
			  <h4><a href="#">学生医疗管理规定</a></h4>
			  <p>广东工业大学学生参加城镇居民医保普通门(急)诊医疗管理暂行办法 根据《广州市城镇居民基本医疗保险试行办法》（穗府办[2008]22号）和《关于调整<广州市城镇居民基本医疗保险试行办法>有关规定的通知》（穗劳社[2009]5号）精神，我校学生已纳入城镇居民基本医疗保险，并选用……</p>
			</div>
			<div class="intro">
			  <h4><a href="#">转诊制度及选定医疗机构</a></h4>
			  <p>转诊制度及选定医疗机构（一）转诊制度：原则上我校教工、参保学生患病须先到校医院就诊（急诊除外），先经校医院医生处理，由于条件所限或病情所需应转到大医院诊疗的，由接诊医生开具转诊单，转诊到校外门诊选定医疗机构就诊，所产生的医疗费用按规定比例报销……</p>
			</div>
			<div class="last intro">
			  <h4><a href="#">处方规定</a></h4>
			  <p>处方制度（修订） 一、 处方权限 1．具有执业医师资格的在职、外聘、返聘医师均有处方权，有处方权的医师应将签字式样存放药剂科作鉴。2．无处方权的见习医师须在带教医师指导下开具处方，其处方由带教医师审签后生效。3．麻醉药品处方应由主治医师以上医师或经院领导批准授予……</p>
			</div>
		  </div>
		</div>
	  </div>
	</div>
