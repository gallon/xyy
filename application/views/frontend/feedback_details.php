    <!-- content -->
	<div class="content">
	  <div class="title">
		<h1 class="Chtitle">交流平台</h1>
		<h4 class="Entitle">Communication Platform</h4>
	  </div>
	  <div class="wrap">
		<div class="part">
		  <ul>
			<a class="feedback part_other" href="<?php echo base_url('feedback'); ?>"><li>意见反馈</li></a>
		  </ul>
		</div>
		<div class="communication">
		  <div class="cur">当前位置：<a href="#">首页</a> > <a href="#">交流平台</a> > <a href="#">意见反馈</a> > 详情</div>
		  <h2>意见反馈</h2>
		  <div class="details">
			<h4><?php echo $feedback->title; ?></h4>
			<p>[姓名]：<?php echo $feedback->fullname; ?></p>
			<p>[时间]：<?php echo $feedback->created_at; ?></p>
			<p>[内容]：<?php echo $feedback->content; ?></p>
            <?php if ($feedback->status == 1): ?>
			  <p>[管理员回复]：<?php echo $feedback->comment; ?></p>
            <?php else: ?>
              <p><strong>未回复</strong></p>
            <?php endif; ?>
		  </div>
		</div>
	  </div>
	</div>
