    <!-- Links -->
    <div class="nav_block friends_link">
      <div class="links_wrap">
	    <div class="titlebg">
	      <h2>友情链接</h2>
	    </div>
	    <ul>
          <?php
          $count = 0;
          shuffle($links);
          ?>
          <?php foreach ($links as $link): ?>
            <?php
             if ($count >= 5)
            {
                break;
            }
            $count ++;
            ?>
	        <li>
	          <a href="<?php echo prep_url($link->url_link); ?>"><?php echo $link->title; ?></a>
	        </li>
          <?php endforeach; ?>
	    </ul>
      </div>
    </div>
    <div class="footer" >
      <ul>
	    <li>
          <a href="http://xyy.gdut.edu.cn">旧版网站</a>
		  <span>|</span>
	    </li>
	    <li>
          <a href="<?php echo base_url('backend'); ?>">后台管理</a>
        </li>
      </ul>
      <p>Copyright&nbsp©&nbsp2003-2014&nbsp&nbsp广东工业大学医院&nbsp&nbsp版权所有</p>
    </div>
  </body>
  <script src="<?php echo base_url('static/frontend/js/index.js'); ?>"></script>
  <script src="<?php echo base_url('static/frontend/js/jquery.simplePagination.js'); ?>"></script>
</html>
