<!DOCTYPE html>
<html lang="zh">
  <head>
    <title>广东工业大学校医院</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="<?php echo base_url('static/frontend/css/style.css'); ?>" type="text/css" media="screen">
    <link rel="stylesheet" href="<?php echo base_url('static/frontend/css/simplePagination.css'); ?>" type="text/css" media="screen">
    <script src="<?php echo base_url('static/frontend/js/jquery.js'); ?>"></script>
  </head>
  <body id="page1">
    <!-- header -->
	<div class="header">
	  <a href="<?php echo base_url(); ?>" id="logo"><img src="<?php echo base_url('static/frontend/images/head.jpg'); ?>"></a>
	  <div class="bg_wrap">
		<div class="nav_wrap">
		  <ul class="nav_menu">
			<span class="bar"></span>
			<li>
			  <a <?php if ($page_nav == 1) { echo 'class="current"'; } ?> href="<?php echo base_url(); ?>">&gt医院主页</a>
			  <ul>
			  </ul>
			</li>
            <?php
            /* parent */
            $intro = array();
            $news = array();
            $health = array();
            $exchange = array();
            /* Another parent which act like children */
            $guide = array();
            $social = array();
            $department = array();

            foreach ($categories as $category)
            {
                $rv = $category['parent'];
                $parent = $rv['category'];
                /* $children = $rv['category']; */

                /* FIXME parent_id => constant */
                switch ($parent->id)
                    {
                    case 1:
                        $intro = $category;
                        break;
                    case 2:
                        $news = $category;
                        break;
                    case 3:
                        $health = $category;
                        break;
                    case 4:
                        $exchange = $category;
                        break;
                    case 5:
                        $guide = $category;
                        break;
                    }
            }
            ?>

			<span class="bar"></span>
			<li><a <?php if ($page_nav == 2) { echo 'class="current"'; } ?> href="#">&gt医院概况</a>
			  <ul>
				<li><a href="<?php echo base_url('intro/hospital'); ?>">医院简介</a></li>
				<li><a href="<?php echo base_url('intro/expert'); ?>">专家介绍</a></li>
				<li><a href="<?php echo base_url('intro/doctor'); ?>">医护人员</a></li>
                <?php foreach ($intro['children'] as $rv): ?>
				  <li><a href="<?php echo base_url('intro/' . $rv['category']->id); ?>"><?php echo $rv['category']->title; ?></a></li>
                <?php endforeach; ?>
			  </ul>
			</li>
			<span class="bar"></span>
			<li>
			  <a <?php if ($page_nav == 3) { echo 'class="current"'; } ?> href="#">&gt医院新闻</a>
			  <ul>
                <?php foreach ($news['children'] as $rv): ?>
				  <li><a href="<?php echo base_url('news/' . $rv['category']->id); ?>"><?php echo $rv['category']->title; ?></a></li>
                <?php endforeach; ?>
			  </ul>
			</li>
			<span class="bar"></span>
			<li>
			  <a <?php if ($page_nav == 4) { echo 'class="current"'; } ?> href="#">&gt就医指南</a>
			  <ul>
                <?php foreach ($guides as $rv): ?>
				  <li><a href="<?php echo base_url('guide/' . $rv['article']->id); ?>"><?php echo $rv['article']->title; ?></a></li>
                <?php endforeach; ?>
                <li><a href="<?php echo base_url('guide/traffic'); ?>">交通指南</a></li>
			  </ul>
			</li>
			<span class="bar"></span>
			<li>
			  <a <?php if ($page_nav == 5) { echo 'class="current"'; } ?> href="<?php echo base_url('department'); ?>">&gt科室介绍</a>
			  <ul>
			  </ul>
			</li>
			<span class="bar"></span>
			<li>
			  <a <?php if ($page_nav == 6) { echo 'class="current"'; } ?> href="#">&gt健康保健</a>
              <?php if (count($health)): ?>
                <ul>
                  <?php foreach ($health['children'] as $rv): ?>
				    <li><a href="<?php echo base_url('health/' . $rv['category']->id); ?>"><?php echo $rv['category']->title; ?></a></li>
                  <?php endforeach; ?>
			    </ul>
              <?php endif; ?>
			</li>
			<span class="bar"></span>
			<li>
			  <a <?php if ($page_nav == 7) { echo 'class="current"'; } ?> href="<?php echo base_url('social'); ?>">&gt公费医疗</a>
			  <ul>
			  </ul>
			</li>
			<span class="bar"></span>
			<li>
			  <a <?php if ($page_nav == 8) { echo 'class="current"'; } ?> href="#">&gt交流平台</a>
			  <ul>
				<li><a href="<?php echo base_url('feedback'); ?>">意见反馈</a></li>
                <?php foreach ($exchange['children'] as $rv): ?>
				  <li><a href="<?php echo base_url('exchange/' . $rv['category']->id); ?>"><?php echo $rv['category']->title; ?></a></li>
                <?php endforeach; ?>
			  </ul>
			</li>
			<span class="bar"></span>
		  </ul>
		</div>
	  </div>
	</div>
    <!-- / header -->
