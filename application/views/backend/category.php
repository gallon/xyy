<div id="page-wrapper">
  <div class="row">
	<div class="col-lg-12">
	  <h1 class="page-header">文章分类管理
		<small> 文章分类添加, 修改, 删除</small>
	  </h1>
	</div>
  </div>
  <div class="panel panel-info">
	<div class="panel-body">
	  <?php if (isset($alert_error)): ?>
		<div class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		  <strong><?php echo $alert_error; ?></strong>
		</div>
	  <?php elseif (isset($alert_success)): ?>
		<div class="alert alert-success alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		  <strong><?php echo $alert_success; ?></strong>
		</div>
	  <?php endif; ?>
	  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<?php foreach ($categories as $category): ?>
		  <?php
		   $parent = $category['parent'];
		  $children = $category['children'];
		  ?>
		  <?php if ($parent['category']->is_parent == 1): ?>
			<div class="panel panel-primary">
			  <div class="panel-heading">
				<h4 class="panel-title">
				  <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $parent['category']->id; ?>">
					<?php echo $parent['category']->title; ?>
				  </a>
				</h4>
			  </div>

			  <div id="collapse<?php echo $parent['category']->id; ?>" class="panel-collapse collapse in" role="tabpanel">
				<div class="panel-body">
				  <div class="row">
					<div class="col-md-6">
					  <form action="<?php echo base_url("backend/category/add"); ?>" method="POST" role="form">
						<div class="input-group">
						  <span class="input-group-addon">添加次级分类</span>
						  <input type="hidden" name="parent-id" value="<?php echo $parent['category']->id; ?>">
						  <input type="text" name="title" class="form-control" placeholder="分类名称">
						  <span class="input-group-btn">
							<button class="btn btn-success" id="add" type="submit">添加</button>
						  </span>
						</div>
					  </form>
					</div>
				  </div>
				  <br/>
				  <ul class="list-group">
					<?php foreach ($children as $child): ?>
					  <li class="list-group-item category-list">
						<div class="row">
						  <div class="col-md-6">
							<h4 class="text-primary"><?php echo $child['category']->title; ?></h4>
							<p class="text-success">文章数量: <span class="badge"><?php echo $child['article_count']; ?></span></p>
						  </div>
						  <div class="col-md-3">
							<input type="hidden" value="<?php echo $child['category']->id; ?>" class="category-id">
							<input type="hidden" value="<?php echo $child['category']->title; ?>" class="category-title">
							<button class="btn btn-warning modify-button" data-toggle="modal" data-target="#modify-modal">修改分类</button>

						  </div>
						  <div class="col-md-3">
							<input type="hidden" value="<?php echo $child['category']->id; ?>" class="category-id">
							<button class="btn btn-danger delete-button" data-toggle="modal" data-target="#delete-modal">删除分类</button>
						  </div>
						</div>
					  </li>
					<?php endforeach; ?>
				  </ul>
				</div>
			  </div>
			</div>
		  <?php else: ?>
			<br/>
			<ul class="list-group">
			  <li class="list-group-item category-list">
				<div class="row">
				  <div class="col-md-6">
					<h4 class="text-primary"><?php echo $parent['category']->title; ?></h4>
					<p class="text-success">文章数量: <span class="badge"><?php echo $child['article_count']; ?></span></p>
				  </div>
				</div>
			  </li>
			</ul>
		  <?php endif; ?>
		<?php endforeach; ?>
	  </div>
	</div>
  </div>
</div>
<div class="modal fade" id="modify-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel">修改分类</h4>
	  </div>
	  <div class="modal-body">
		<form role="form" action="<?php echo base_url('backend/category/modify'); ?>" method="POST" id="modify-form">
		  <input type="hidden" id="modify-category-id" name="category-id" value="">
		  <div class="input-group input-group-lg">
			<span class="input-group-addon">分类名称</span>
			<input type="text" class="form-control" id="category-title" name="category-title" value="">
		  </div>
		</form>
		<div class="modal-footer">
		  <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
		  <button type="button" class="btn btn-warning" id="modify-submit">确定修改</button>
		</div>
	  </div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
  </div>
</div>


<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel">确定删除分类?</h4>
	  </div>
	  <div class="modal-body">
		<p>删除分类后<b class="text-danger">无法恢复</b>,确定要删除此分类?</p>
	  </div>
	  <div class="modal-footer">
		<form action="<?php echo base_url("backend/category/delete"); ?>" method="POST" name="delete-form">
		  <input type="hidden" name="category-id" id="delete-category-id" value="">
		  <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
		  <button type="submit" class="btn btn-danger">确定删除</button>
		</form>
	  </div>
	</div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
