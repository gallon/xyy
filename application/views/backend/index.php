<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">后台首页</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <div class="list-group">
    <a href="#" class="list-group-item">
      <i class="fa fa-comment fa-fw"></i> 用户IP:
      <span class="pull-right text-muted small"><em><?php echo $_SERVER['SERVER_ADDR']; ?></em>
      </span>
    </a>
    <a href="#" class="list-group-item">
      <i class="fa fa-twitter fa-fw"></i> 上次登录
      <span class="pull-right text-muted small"><em><?php echo $user->updated_at; ?></em>
      </span>
    </a>
    <a href="#" class="list-group-item">
      <i class="fa fa-envelope fa-fw"></i> 文章总数
      <span class="pull-right text-muted small"><em><?php echo $article_count; ?></em>
      </span>
    </a>
    <a href="#" class="list-group-item">
      <i class="fa fa-users fa-fw"></i> 用户总数
      <span class="pull-right text-muted small"><em><?php echo $user_count; ?></em>
      </span>
    </a>
    <a href="#" class="list-group-item">
      <i class="fa fa-tasks fa-fw"></i> 游客反馈
      <?php
      $bad_feedbacks = $feedbacks['bad'];
      $bad_count = count($bad_feedbacks);
      ?>
      <?php if ($bad_count > 0): ?>
        <span class="pull-right text-muted small text-danger">有 <?php echo $bad_count; ?> 条新反馈!<em></em>
        </span>
      <?php else: ?>
        <span class="pull-right text-muted small text-success">所有反馈都已经处理!<em></em>
        </span>
      <?php endif; ?>
    </a>
  </div>
  <!-- /.row -->
</div>
<!-- /#page-wrapper -->
