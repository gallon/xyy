<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>广东工业大学校医院后台管理</title>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/bootstrap.css'); ?>" />
    <!-- Add custom CSS here -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/font-awesome.min.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('static/css/sb-admin.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('static/frontend/css/simplePagination.css'); ?>" type="text/css" media="screen">
    <script type="text/javascript" src="<?php echo base_url('static/js/jquery.js'); ?>"></script>
  </head>
  <body>
    <div id="wrapper">
      <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
          <a class="navbar-brand" href="<?php echo base_url('backend'); ?>">广东工业大学校医院后台管理</a>
        </div>
        <!-- /.navbar-header -->
        <ul class="nav navbar-top-links navbar-right">
          <!-- /.dropdown -->
          <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
              <i class="fa fa-user fa-fw"></i>  <?php echo $user->username; ?> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
              <li><a href="#" data-toggle="modal" data-target="#user-modal"><i class="fa fa-user fa-fw"></i> 个人中心</a>
              </li>
              <li class="divider"></li>
              <li><a href="<?php echo base_url('user/logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> 登出账户</a>
              </li>
            </ul>
            <!-- /.dropdown-user -->
          </li>
          <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->
        <div class="navbar-default navbar-static-side" role="navigation">
          <div class="sidebar-collapse">
            <ul class="nav" id="side-menu">
              <li>
                <a href="<?php echo base_url('backend/dashboard'); ?>"><i class="fa fa-dashboard fa-fw"></i> 后台首页</a>
              </li>
              <li>
                <a href="#"><i class="fa fa-tag fa-fw"></i> 医院概况<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                  <li>
                    <a href="<?php echo base_url('backend/introduce'); ?>"> 医院介绍</a>
                  </li>
                  <li>
                    <a href="<?php echo base_url('backend/doctor'); ?>"> 医生管理</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="#"><i class="fa fa-pencil fa-fw"></i> 文章与分类<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                  <li>
                    <a href="#"> 文章管理<span class="fa arrow"></span></a>
                    <ul class="nav nav-third-level">
                      <li>
                        <a href="<?php echo base_url('backend/article/add'); ?>"> 发表文章</a>
                      </li>
                      <li>
                        <a href="<?php echo base_url('backend/article'); ?>"> 文章列表</a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="<?php echo base_url('backend/category'); ?>"> 分类管理</a>
                  </li>
                </ul>
                <!-- /.nav-second-level -->
              </li>

              <li>
                <a href="<?php echo base_url('backend/users'); ?>"><i class="fa fa-users fa-fw"></i> 用户管理</a>
              </li>
              <li>
                <a href="<?php echo base_url('backend/feedback'); ?>"><i class="fa fa-users fa-fw"></i> 反馈管理</a>
              </li>
              <li>
                <a href="<?php echo base_url('backend/link'); ?>"><i class="fa fa-external-link fa-fw"></i> 友情链接</a>
              </li>
            </ul>
            <!-- /#side-menu -->
          </div>
          <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
      </nav>
      <div class="modal fade" id="user-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">用户个人中心</h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal" role="form" action="<?php echo base_url("backend/user/modify"); ?>" method="POST" id="user-modify-form">
                <div class="form-group">
                  <label for="username" class="col-sm-2 control-label">用户名</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="username" name="username" value="<?php echo $user->username; ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">当前密码</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="password" placeholder="请输入当前密码确认修改">
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">新密码</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="new-password" placeholder="如不需修改密码，请勿输入">
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-sm-2 control-label">确认密码</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="password-check" placeholder="如不需修改密码，请勿输入">
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
              <button type="button" class="btn btn-warning" id="user-modify-submit">确认修改</button>
            </div>
          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div>
