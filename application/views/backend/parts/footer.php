    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="<?php echo base_url('static/js/jquery.metisMenu.js'); ?>"></script>
    <script src="<?php echo base_url('static/js/bootstrap.js'); ?>"></script>
    <script src="<?php echo base_url('static/js/sb-admin.js'); ?>"></script>
    <script src="<?php echo base_url('static/ckeditor/ckeditor.js'); ?>"></script>
    <script src="<?php echo base_url('static/ckfinder/ckfinder.js'); ?>"></script>
    <script src="<?php echo base_url('static/js/dashboard.js'); ?>"></script>
    <script src="<?php echo base_url('static/js/editor.js'); ?>"></script>
    <script src="<?php echo base_url('static/frontend/js/jquery.simplePagination.js'); ?>"></script>
  </body>
</html>
