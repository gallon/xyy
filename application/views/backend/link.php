<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">友情链接</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <div class="panel panel-info">
    <div class="panel-body">
      <button class="btn btn-primary" data-toggle="modal" data-target="#addLink">添加</button>
      <hr/>
      <ul class="list-group">
        <?php foreach ($links as $link): ?>
          <li class="list-group-item">
            <div class="row">
              <div class="col-md-6">
                <h4 class="list-group-item-heading "><p class="text-primary"><?php echo $link->title; ?></p></h4>
                <div class="text-success"> 网址:
                  <span class="text-warning"> <?php echo $link->url_link; ?></span>
                </div>
              </div>
              <div class="col-md-6">
                <a class="btn btn-danger" href="<?php echo base_url('backend/link/delete/'.$link->id); ?>">删除</a>
              </div>
            </div>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </div>
</div>

<div class="modal fade" id="addLink" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">添加用户</h4>
      </div>
      <form action="<?php echo base_url("backend/link/add"); ?>" method="POST" id="add-link-form">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <p>
                <div class="input-group">
                  <span class="input-group-addon"><b>名称</b></span>
                  <input type="text" class="form-control" name="title" id="title">
                </div>
              </p>
              <p>
                <div class="input-group">
                  <span class="input-group-addon"><b>网址</b></span>
                  <input type="text" class="form-control" id="url" name="url">
                </div>
              </p>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
          <button id="submit" type="submit" class="btn btn-success">添加</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
