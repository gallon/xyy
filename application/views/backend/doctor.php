<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">医生管理</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>

  <div class="panel panel-info">
    <div class="panel-body">
      <?php if (isset($alert_error)): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <strong><?php echo $alert_error; ?></strong>
        </div>
      <?php elseif (isset($alert_success)): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <strong><?php echo $alert_success; ?></strong>
        </div>
      <?php endif; ?>

      <a href="<?php echo base_url('backend/doctor/add'); ?>" class="btn btn-primary">添加医生</a>
      <hr/>
      <?php foreach ($doctors as $doctor): ?>
        <div class="list-group">
          <a href="<?php echo base_url('backend/doctor/' . $doctor->id); ?>" class="list-group-item">
            <div class="row">
              <div class="col-md-2">
                <img data-src="" alt="" src="<?php echo $doctor->portrait; ?>" style="height: 140px; width: 140px;" class="img-rounded">
              </div>
              <div class="col-md-6">
                <h4 class="text-success"> 姓名:
                  <span class="text-warning"> <?php echo $doctor->fullname; ?></span>
                </h4>
                <h4 class="text-success"> 职位:
                  <span class="text-warning"> <?php echo $doctor->position; ?></span>
                </h4>
                <h4 class="text-success"> 校区:
                  <span class="text-warning"> <?php echo $doctor->campus; ?></span>
                </h4>
                <h4 class="text-success"> 添加时间:
                  <span class="text-warning">  <?php echo $doctor->created_at; ?></span>
                </h4>
                <?php if ($doctor->expert == 1): ?>
                  <span class="label label-danger"> 专家</span>
                <?php endif; ?>
              </div>
            </div>
          </a>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>
