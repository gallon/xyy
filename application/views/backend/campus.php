<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">校区管理
      </h1>
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-body">
      <?php if (isset($alert_error)): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <strong><?php echo $alert_error; ?></strong>
        </div>
      <?php elseif (isset($alert_success)): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <strong><?php echo $alert_success; ?></strong>
        </div>
      <?php endif; ?>

      <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-primary">
          <div class="panel-body">
            <form action="<?php echo base_url("backend/campus/add"); ?>" method="POST" role="form">
              <div class="row">
                <div class="col-md-4">
                  <div class="input-group">
                    <span class="input-group-addon">新校区</span>
                    <input type="text" name="title" class="form-control" placeholder="名称">
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="input-group">

                    <input type="text" name="work_time" class="form-control" placeholder="办公时间">
                    <span class="input-group-btn">
                      <button class="btn btn-success" id="add" type="submit">添加</button>
                    </span>
                  </div>
                </div>
              </div>
            </form>
            <br/>
            <ul class="list-group">
              <?php foreach ($campuses as $campus): ?>
                <li class="list-group-item campus-list">
                  <div class="row">
                    <div class="col-md-6">
                      <h4 class="text-primary"><?php echo $campus->title; ?></h4>
                      <p class="text-success">办公时间: <span class="badge"><?php echo $campus->work_time; ?></span></p>
                    </div>
                    <div class="col-md-3">
                      <input type="hidden" value="<?php echo $campus->id; ?>" class="campus-id">
                      <input type="hidden" value="<?php echo $campus->title; ?>" class="campus-title">
                      <input type="hidden" value="<?php echo $campus->work_time; ?>" class="campus-work-time">

                      <button class="btn btn-warning modify-button  btn-lg btn-block" data-toggle="modal" data-target="#modify-modal">修改</button>
                    </div>
                    <div class="col-md-3">
                      <input type="hidden" value="<?php echo $campus->id; ?>" class="campus-id">
                      <button class="btn btn-danger delete-button btn-lg btn-block" data-toggle="modal" data-target="#delete-modal">删除</button>
                    </div>
                  </div>
                </li>
              <?php endforeach; ?>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modify-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">修改分类</h4>
      </div>
      <div class="modal-body">
        <form role="form" action="<?php echo base_url('backend/campus/modify'); ?>" method="POST" id="modify-form">
          <input type="hidden" id="campus-id" name="id" value="">
          <div class="input-group input-group-lg">
            <span class="input-group-addon">校区名称</span>
            <input type="text" class="form-control" id="campus-title" name="title" value="">
          </div>
          <hr/>
          <div class="input-group input-group-lg">
            <span class="input-group-addon">办公时间</span>
            <input type="text" class="form-control" id="campus-work-time" name="work_time" value="">
          </div>
        </form>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
          <button type="button" class="btn btn-warning" id="modify-submit">确定修改</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div>
</div>


<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">确定删除分类?</h4>
      </div>
      <div class="modal-body">
        <p>删除分类后<b class="text-danger">无法恢复</b>,确定要删除此分类?</p>
      </div>
      <div class="modal-footer">
        <form action="<?php echo base_url("backend/campus/delete"); ?>" method="POST" name="delete-form" id="delete-form">
          <input type="hidden" name="id" id="campus-id" value="">
          <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
          <button type="submit" class="btn btn-danger">确定删除</button>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
