<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">添加文章</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <div class="panel panel-info">
    <div class="panel-body">
      <form action="<?php echo base_url("backend/article/add"); ?>" method="post">
        <p>
          <div class="input-group">
            <span class="input-group-addon"><b>标题</b></span>
            <input type="text" class="form-control" name="title" placeholder="Title">
          </div>
        </p>
        <p>
          <div class="input-group">
            <span class="input-group-addon"><b>文章分类</b></span>
            <select class="form-control" name="category">
              <?php foreach ($categories as $category): ?>
                <?php
                $parent = $category['parent'];
                $children = $category['children'];
                ?>
                <?php if ($parent['category']->is_parent == 0): ?>
                  <option value="<?php echo $parent['category']->id; ?>"><?php echo $parent['category']->title; ?></option>
                <?php else: ?>
                  <option disabled><?php echo $parent['category']->title; ?></option>
                <?php endif; ?>
                <?php foreach ($children as $child): ?>
                  <option value="<?php echo $child['category']->id; ?>"><?php echo $child['category']->title; ?></option>
                <?php endforeach; ?>
              <?php endforeach; ?>
            </select>
          </div>
        </p>
	    <p>
		  <textarea class="ckeditor" cols="80" id="editor1" name="content" rows="50">
		  </textarea>
	    </p>
	    <p>
          <button type="submit" class="btn btn-success">提交</button>
	    </p>
	  </form>
    </div>
  </div>
</div>

