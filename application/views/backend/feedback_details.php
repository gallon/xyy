<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">回复反馈</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <div class="panel panel-info">
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6 col-md-offset-1">
          <div>
            <div class="input-group">
              <span class="input-group-addon"><b>姓名</b></span>
              <input type="text" class="form-control" name="fullname" value="<?php echo $feedback->fullname; ?>" disabled>
            </div>
            <br/>
            <div class="input-group">
              <span class="input-group-addon"><b>E-mail</b></span>
              <input type="text" class="form-control" name="email" value="<?php echo $feedback->email; ?>" disabled>
            </div>
            <br/>
            <div class="input-group">
              <span class="input-group-addon"><b>标题</b></span>
              <input type="text" class="form-control" name="title" value="<?php echo $feedback->title; ?>" disabled>
            </div>
            <br/>
            <div class="input-group">
              <span class="input-group-addon"><b>内容</b></span>
              <input type="text" class="form-control" name="content" value="<?php echo $feedback->content; ?>" disabled>
            </div>
            <?php if ($feedback->status == 0): ?>
              <h3><span class="label label-danger">未回复</span></h3>
            <?php else: ?>
              <h3><?php echo $handler->username; ?> <span class="label label-success">已回复</span></h3>
            <?php endif; ?>
            <hr/>
            <form action="<?php echo base_url("backend/feedback/comment"); ?>" method="POST">
              <input type="hidden" name="id" value="<?php echo $feedback->id; ?>">
              <div class="input-group">
                <span class="input-group-addon"><b>回复内容</b></span>
                <textarea class="form-control" rows="5" name="comment" ><?php echo $feedback->comment; ?></textarea>
              </div>
              <br/>
              <button type="submit" class="btn btn-success btn-block">回复</button>
              <a class="btn btn-danger btn-block" href='<?php echo base_url("backend/feedback/delete/" . $feedback->id); ?>'">删除</a>
              <a class="btn btn-default btn-block" href='<?php echo base_url("backend/feedback"); ?>'">取消</a>
	        </form>
          </div>
        </div>
      </div>
    </div>
  </div>
