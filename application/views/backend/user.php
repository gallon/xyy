<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">用户管理</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>

  <div class="panel panel-info">
    <div class="panel-body">
      <?php if (isset($alert_error)): ?>
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <strong><?php echo $alert_error; ?></strong>
        </div>
      <?php elseif (isset($alert_success)): ?>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <strong><?php echo $alert_success; ?></strong>
        </div>
      <?php endif; ?>

      <button class="btn btn-primary" data-toggle="modal" data-target="#addUser">增加用户</button>
      <script type="text/javascript" language="javascript">
        function checkinput()
        {
            var username = document.addUser.username.value;
            var password = document.addUser.password.value;
            var password_check = document.addUser.passwordcheck.value;
            if((username<=0)||(password<=0)||(password_check<=0)||(password!=password_check))
            {
                document.getElementById("submit").disabled=true;
            }
            else
            {
                document.getElementById("submit").disabled=false;
            }
        }
      </script>
      <hr/>
      <?php foreach ($users as $user): ?>
        <div class="list-group">
          <a href="#" class="list-group-item">
            <h4 class="list-group-item-heading "><p class="text-primary"><?php echo $user->username; ?></p></h4>
            <div class="text-success"> 注册时间:
              <span class="text-warning"> <?php echo $user->created_at; ?></span>
            </div>
          </a>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</div>
<div class="modal fade" id="addUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">添加用户</h4>
      </div>
      <form action="<?php echo base_url("backend/users/add"); ?>" method="post" name="addUser">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <p>
                <div class="input-group">
                  <span class="input-group-addon"><b>用户名</b></span>
                  <input type="text" class="form-control" name="username" id="username" placeholder="登陆时使用的名称" onKeyUp="checkinput()">
                </div>
              </p>
              <p>
                <div class="input-group">
                  <span class="input-group-addon"><b>密码</b></span>
                  <input type="password" class="form-control" id="password" name="password" placeholder="输入密码" onKeyUp="checkinput()">
                </div>
              </p>
              <p>
                <div class="input-group">
                  <span class="input-group-addon"><b>确认密码</b></span>
                  <input type="password" class="form-control" id="passwordcheck" name="passwordcheck" placeholder="再次输入密码" onKeyUp="checkinput()">
                </div>
              </p>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
          <button id="submit" type="submit" class="btn btn-success" disabled="true" >添加</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
