<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">文章管理
        <small> 文章发布, 修改, 删除</small>
      </h1>
    </div>
  </div>
  <div class="panel panel-success">
    <div class="panel-heading">
      <?php foreach ($categories as $category): ?>
        <?php
         $parent = $category['parent'];
        $children = $category['children'];
        ?>
        <ul class="nav nav-pills" role="tablist">
          <?php if ($parent['category']->is_parent == 0): ?>
            <li role="presentation" <?php if ($page_id == $parent['category']->id) {echo 'class="active"';} ?>><a href="<?php echo base_url('backend/article/cate/'.$parent['category']->id); ?>"><?php echo $parent['category']->title; ?> <span class="badge"><?php echo $parent['article_count']; ?></span></a></li>
          <?php else: ?>
            <li role="presentation" class="disabled"><a href="#"><?php echo $parent['category']->title; ?></a></li>
          <?php endif; ?>
          <?php foreach ($children as $child): ?>
            <li role="presentation" <?php if ($page_id == $child['category']->id) {echo 'class="active"';} ?>><a href="<?php echo base_url('backend/article/cate/'.$child['category']->id); ?>"><?php echo $child['category']->title; ?> <span class="badge"><?php echo $child['article_count']; ?></span></a></li>
          <?php endforeach; ?>
        </ul>
        <hr/>
      <?php endforeach; ?>
    </div>
    <divx class="panel-body">
      <div class="list-group">
        <?php foreach ($articles as $rv): ?>
          <hr/>
          <?php
          $article = $rv['article'];
          $category = $rv['category'];
          $author = $rv['author'];
          ?>
          <a href="<?php echo base_url('backend/article/'.$article->id); ?>" class="list-group-item">
            <h4 class="list-group-item-heading "><p class="text-primary"><?php echo $article->title; ?></p></h4>
            <div class="row">
              <div class="col-md-3">
                <div class="text-success"> 分类:
                  <span class="text-warning"> <?php echo $category->title; ?></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="text-success"> 发表时间:
                  <span class="text-warning"> <?php echo $article->created_at; ?></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="text-success"> 更新时间:
                  <span class="text-warning"> <?php echo $article->updated_at; ?></span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="text-success"> 最近操作者:
                  <span class="text-warning"> <?php echo $author->username;?> </span>
                </div>
              </div>
            </div>
          </a>
        <?php endforeach; ?>
      </div>
      <div class="row col-md-offset-4 col-md-4">
        <div class="pagination">
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(function() {
      $('.pagination').pagination({
          items: <?php echo $all_count; ?>,
          /* TODO Article count */
          itemsOnPage: 10,
          prevText: '前一页',
          nextText: '后一页',
          currentPage: <?php echo $page; ?>,
          cssStyle: 'compact-theme',
          onPageClick: function (pageNumber, event) {
              <?php
              if ($page_id == 0)
              {
                  echo 'location.href = "/backend/article/page/" + pageNumber;';
              }
              else
              {
                  echo 'location.href = "/backend/article/cate/' . $page_id .'/" + pageNumber;';
              }
              ?>
          }
      });
  });
</script>
