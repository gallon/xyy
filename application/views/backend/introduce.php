<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header"> 医院介绍</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <div class="panel panel-info">
    <div class="panel-body">
      <form action="<?php echo base_url("backend/introduce/submit"); ?>" method="post" role="form">
        <p>
          <textarea class="ckeditor" cols="80" id="editor1" name="content" rows="50">
            <?php if(isset($introduce)) echo $introduce->content; ?>
          </textarea>
        </p>
        <p>
          <button type="submit" class="btn btn-warning">修改</button>
        </p>
      </form>
    </div>
  </div>
</div>
