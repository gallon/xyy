<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">反馈管理
        <small> 处理留言</small>
      </h1>
    </div>
  </div>
  <div class="panel panel-success">
    <div class="panel-body">
      <div class="list-group">
        <?php foreach ($feedbacks as $rv): ?>
          <?php
           $feedback = $rv['feedback'];
          $handler = $rv['handler'];
          ?>
          <a href="<?php echo base_url('backend/feedback/details/'.$feedback->id); ?>" class="list-group-item">
            <h4 class="list-group-item-heading "><p class="text-primary"><?php echo $feedback->title; ?></p></h4>
            <div class="row">
              <div class="col-md-3">
                <div class="text-success"> 发表时间:
                  <span class="text-warning"> <?php echo $feedback->created_at; ?></span>
                </div>
              </div>
              <?php if ($feedback->status == 0): ?>
                <div class="col-md-3">
                  <div class="text-success"> 状态:
                    <span class="text-danger"> 未处理</span>
                  </div>
                </div>
              <?php else: ?>
                <div class="col-md-3">
                  <div class="text-success"> 状态:
                    <span class="text-success"> 已处理</span>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="text-success"> 处理时间:
                    <span class="text-warning"> <?php echo $feedback->updated_at; ?></span>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="text-success"> 最近操作者:
                    <span class="text-warning"> <?php echo $handler->username;?> </span>
                  </div>
                </div>
              <?php endif; ?>
            </div>
          </a>
        <?php endforeach; ?>
      </div>
      <div class="row col-md-offset-4 col-md-4">
        <div class="pagination">
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(function() {
      $('.pagination').pagination({
          items: <?php echo $all_count; ?>,
          /* TODO Article count */
          itemsOnPage: 5,
          prevText: '前一页',
          nextText: '后一页',
          currentPage: <?php echo $page; ?>,
          cssStyle: 'compact-theme',
          onPageClick: function (pageNumber, event) {
              location.href = "/backend/feedback/" + pageNumber;
          }
      });
  });
</script>
