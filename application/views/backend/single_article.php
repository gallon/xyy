<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">修改文章</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <div class="panel panel-info">
    <div class="panel-body">
      <form action="<?php echo base_url("backend/article/modify"); ?>" method="post">
        <input type="hidden" name="article" value="<?php echo $article->id; ?>"/>
        <p>
          <div class="input-group">
            <span class="input-group-addon"><b>标题</b></span>
            <input type="text" class="form-control" name="title" placeholder="Title" value="<?php echo $article->title; ?>">
          </div>
        </p>
        <p>
          <div class="input-group">
            <span class="input-group-addon"><b>文章分类</b></span>
            <select class="form-control" name="category">
              <?php foreach ($categories as $category): ?>
                <?php
                 $parent = $category['parent'];
                $children = $category['children'];
                ?>
                <?php if ($parent['category']->is_parent == 0): ?>
                  <option value="<?php echo $parent['category']->id; ?>" <?php if ($parent['category']->id == $article->category_id) {echo 'selected';}?>><?php echo $parent['category']->title; ?></option>
                <?php else: ?>
                  <option disabled><?php echo $parent['category']->title; ?></option>
                <?php endif; ?>
                <?php foreach ($children as $child): ?>
                  <option value="<?php echo $child['category']->id; ?>" <?php if ($child['category']->id == $article->category_id) {echo 'selected';}?>><?php echo $child['category']->title; ?></option>
                <?php endforeach; ?>
              <?php endforeach; ?>
            </select>
          </div>
        </p>
	    <p>
		  <textarea class="ckeditor" cols="80" id="editor1" name="content" rows="50">
            <?php echo $article->content; ?>
		  </textarea>
	    </p>
	    <p>
          <button type="submit" class="btn btn-warning">保存修改</button>
          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-modal">删除文章</button>
	    </p>
	  </form>
    </div>
  </div>
</div>
<!-- delete-modal -->
<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">删除文章</h4>
      </div>
      <div class="modal-body">
        <form id="delete-form" role="form" action="<?php echo base_url('backend/article/delete'); ?>" method="POST">
          <input type="hidden" name="article" value="<?php echo $article->id; ?>">
        </form>
        <p class="text-danger">删除文章后将无法恢复!</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
        <button id="delete-button" type="button" class="btn btn-danger">确定删除</button>
      </div>
    </div>
  </div>
</div>
