<div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header">添加医生</h1>
    </div>
    <!-- /.col-lg-12 -->
  </div>
  <div class="panel panel-info">
    <div class="panel-body">
      <div class="row">
        <div class="col-md-6 col-md-offset-1">
          <form action="<?php echo base_url("backend/doctor/add"); ?>" method="post">
            <div>
              <b>照片:</b>
              <img id="thumbnail" src="<?php echo base_url('static/upload/images/index.png')?>" alt="" height="140" width="140" class="img-rounded">
              <button type="button" class="btn btn-info" onclick="BrowseServer();">浏览服务器</button>
              <input id="portrait" name="portrait" type="hidden" value="/static/upload/images/index.png">
            </div>
            <br/>
            <div class="input-group">
              <span class="input-group-addon"><b>姓名</b></span>
              <input type="text" class="form-control" name="fullname" value="" placeholder="医生姓名">
            </div>
            <br/>
            <div class="input-group">
              <span class="input-group-addon"><b>职位</b></span>
              <input type="text" class="form-control" name="position" value="" placeholder="医生职位">
            </div>
            <br/>
            <div class="input-group">
              <span class="input-group-addon"><b>电话或手机</b></span>
              <input type="text" class="form-control" name="phone" value="" placeholder="电话或手机">
            </div>
            <br/>
            <div class="input-group">
              <span class="input-group-addon"><b>校区</b></span>
              <input type="text" class="form-control" name="campus" value="" placeholder="校区">
            </div>
            <br/>
            <div class="input-group">
              <span class="input-group-addon"><b>描述</b></span>
              <textarea class="form-control" rows="3" name="description" placeholder="医生描述"></textarea>
            </div>
            <br/>
            <div class="checkbox">
              <label>
                <input type="checkbox" name="expert" value="1">
                <b>是否为专家?</b>
              </label>
            </div>
            <hr/>
            <button type="submit" class="btn btn-success btn-block">添加</button>
            <a class="btn btn-default btn-block" href='<?php echo base_url("backend/doctor"); ?>'">取消</a>
	      </form>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  function BrowseServer()
  {
      // You can use the "CKFinder" class to render CKFinder in a page:
      var finder = new CKFinder();
      finder.selectActionFunction = function (fileUrl, data) {
          document.getElementById( 'portrait' ).value = fileUrl;
          document.getElementById( 'thumbnail' ).src = fileUrl;
      };
      finder.popup();
      // It can also be done in a single line, calling the "static"
      // popup( basePath, width, height, selectFunction ) function:
      // CKFinder.popup( '../', null, null, SetFileField ) ;
      //
      // The "popup" function can also accept an object as the only argument.
      // CKFinder.popup( { basePath : '../', selectActionFunction : SetFileField } ) ;
  }
</script>
