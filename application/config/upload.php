<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Upload
| -------------------------------------------------------------------------
| The file specify basic upload configuration.
*/

$config = array(
    'upload_path' => dirname(BASEPATH) . '/static/uploads',
    'allowed_types' => 'doc|docx|xls|xlsx|ppt|pptx|zip|rar',
    'encrypt_name' => true
);
