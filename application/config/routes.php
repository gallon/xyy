<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$request_method = strtoupper($_SERVER['REQUEST_METHOD']);

$route['default_controller'] = 'frontend/home/index';
$route['404_override'] = '';

// 动态
$route['/'] = 'frontend/home/index';

$route['intro/hospital'] = 'frontend/introduce/hospital';
$route['intro/expert'] = 'frontend/introduce/expert';
$route['intro/doctor'] = 'frontend/introduce/doctor';
$route['intro/doctor/(:num)'] = 'frontend/introduce/doctor_details/$1';
$route['intro/(:num)'] = 'frontend/introduce/other_cate/$1';
$route['intro/(:num)/(:num)'] = 'frontend/introduce/other_cate/$1/$2';

/* $route['news'] = 'frontend/news/index'; */
$route['news/(:any)'] = 'frontend/home/news/$1';
$route['news/(:any)/(:num)'] = 'frontend/home/news/$1/$2';

$route['department'] = 'frontend/home/department';
$route['department/(:num)'] = 'frontend/home/department/$1';

$route['health/(:num)'] = 'frontend/home/health/$1';
$route['health/(:num)/(:num)'] = 'frontend/home/health/$1/$2';

$route['social'] = 'frontend/home/social';
$route['social/(:num)'] = 'frontend/home/social/$1';

$route['guide/(:num)'] = 'frontend/home/guide/$1';
$route['guide/traffic'] = 'frontend/home/guide_traffic';

$route['feedback'] = 'frontend/home/feedback';
$route['feedback/details/(:num)'] = 'frontend/home/feedback_details/$1';
$route['feedback/(:num)'] = 'frontend/home/feedback/$1';
$route['feedback/add'] = 'frontend/home/add_feedback';

$route['exchange/(:num)'] = 'frontend/home/exchange/$1';
$route['exchange/(:num)/(:num)'] = 'frontend/home/exchange/$1/$2';

$route['workflow'] = 'frontend/home/workflow';
$route['public'] = 'frontend/home/category/1';
$route['regulation'] = 'frontend/home/category/2';
$route['secret'] = 'frontend/home/category/3';
$route['service'] = 'frontend/home/category/4';

$route['article/(:num)'] = 'frontend/home/article/$1';

// 后台相关页面
$route['backend'] = 'backend/dashboard';
$route['backend/introduce'] = 'backend/dashboard/introduce';
$route['backend/category'] = 'backend/dashboard/category';
$route['backend/doctor'] = 'backend/dashboard/doctor';
$route['backend/users'] = 'backend/dashboard/user';
$route['backend/link'] = 'backend/dashboard/link';

// 文章相关操作
$route['backend/article'] = 'backend/article/index';
$route['backend/article/page/(:num)'] = 'backend/article/index/$1';
$route['backend/article/cate/(:num)'] = 'backend/article/filter_by_cate/$1';
$route['backend/article/cate/(:num)/(:num)'] = 'backend/article/filter_by_cate/$1/$2';
$route['backend/article/add'] = $request_method  === 'POST' ? 'backend/article/add' : 'backend/article/add_article';
$route['backend/article/(:num)'] = 'backend/article/single_article/$1';
$route['backend/article/modify'] = 'backend/article/modify';
$route['backend/article/delete'] = 'backend/article/delete';

// 介绍相关操作
$route['backend/introduce/submit'] = 'backend/introduce/modify';

// 文章分类相关操作
$route['backend/category'] = 'backend/dashboard/category';
$route['backend/category/modify'] = 'backend/category/modify';
$route['backend/category/delete'] = 'backend/category/delete';

// 医生相关操作
$route['backend/doctor/add'] = $request_method  === 'POST' ? 'backend/doctor/add' : 'backend/doctor/add_doctor';
$route['backend/doctor/(:num)'] = 'backend/doctor/details/$1';
$route['backend/doctor/modify'] = 'backend/doctor/modify';
$route['backend/doctor/delete/(:num)'] = 'backend/doctor/delete/$1';

// Campus
$route['backend/campus'] = 'backend/campus/index';
$route['backend/campus/add'] = 'backend/campus/add';
$route['backend/campus/modify'] = 'backend/campus/modify';
$route['backend/campus/delete'] = 'backend/campus/delete';

// Feedback
$route['backend/feedback'] = 'backend/feedback/index';
$route['backend/feedback/(:num)'] = 'backend/feedback/index/$1';
$route['backend/feedback/details/(:num)'] = 'backend/feedback/details/$1';
$route['backend/feedback/comment'] = 'backend/feedback/update_comment';

// 用户相关操作
$route['backend/users/add'] = 'backend/user/add';
$route['backend/user/modify'] = 'backend/user/modify';

// 用户会话管理相关
$route['user/login'] = $request_method  === 'POST' ? 'backend/authentication/verify_login' : 'backend/authentication/login';
$route['user/logout'] = 'backend/authentication/logout';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
