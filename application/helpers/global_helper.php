<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 自动加载的全局助手函数
 */

/**
 * 生成当前时间的 mysql timestamp 风格时间戳串
 */
if (! function_exists('mysql_timestamp_now'))
{
    function mysql_timestamp_now()
    {
        $mysql_datetime_format = 'Y-m-d H:i:s';
        return date($mysql_datetime_format, time());
    }
}

/**
 * 使用 sha1 对一个字符串进行加密
 */
if (! function_exists('hash_str'))
{
    function hash_str($str)
    {
        return sha1($str);
    }
}
