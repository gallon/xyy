<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Campus operation
 */
class Campus_service {

    /**
     * CI instance
     *
     * @var object
     */
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->model('Campus_model', 'campus_model');
    }

    /**
     * Get all campuss
     *
     * @return array of object
     */
    public function get_all()
    {
        return $this->CI->campus_model->get_all();
    }

    /**
     * Get campus by campus's id
     *
     * @param int $id
     * @return object
     */
    public function get_by_id($id)
    {
        return $this->CI->campus_model->get_by_id($id);
    }

    public function create($title, $work_time)
    {
        $this->CI->campus_model->create(array(
            'title' => $title,
            'work_time' => $work_time
        ));
    }

    public function modify($id, $title, $work_time)
    {
        if ( ! $this->get_by_id($id))
        {
            return;
        }
        $this->CI->campus_model->modify($id, array(
            'title' => $title,
            'work_time' => $work_time
        ));
    }

    public function delete($id)
    {
        if ( ! $this->get_by_id($id))
        {
            return;
        }
        $this->CI->campus_model->delete($id);
    }
}
