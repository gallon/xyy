<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Doctor operation
 */
class Doctor_service {

    /**
     * CI instance
     *
     * @var object
     */
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->model('Doctor_model', 'doctor_model');
    }


    /**
     * Get all doctors
     *
     * @return array of object
     */
    public function get_all()
    {
        return $this->CI->doctor_model->get_all();
    }

    public function get_expert()
    {
        return $this->CI->doctor_model->get_expert();
    }

    public function create($doctor)
    {
        $this->CI->doctor_model->create($doctor);
    }

    /**
     * Get doctor by doctor's id
     *
     * @param int $id
     * @return object
     */
    public function get_by_id($id)
    {
        return $this->CI->doctor_model->get_by_id($id);
    }

    /**
     * Change doctor information
     *
     * @param object $doctor
     * @param array $doctor_info
     * @return bool
     */
    public function modify($doctor, $info)
    {
        $this->CI->doctor_model->modify($doctor, $info);
    }

    public function delete($id)
    {
        $doctor = $this->get_by_id($id);
        if ($doctor)
        {
            $this->CI->doctor_model->delete($doctor);
        }
    }
}
