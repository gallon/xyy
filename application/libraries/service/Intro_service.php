<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Introduce operation
 */
class Intro_service {

    /**
     * CI instance
     *
     * @var object
     */
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->model('Intro_model', 'intro_model');
    }

    public function get_one()
    {
        return $this->CI->intro_model->get_one();
    }

    public function modify($title, $content)
    {
        $this->CI->intro_model->modify(array(
            'title' => $title,
            'content' => $content
        ));
    }
}
