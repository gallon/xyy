<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Link operation
 */
class Link_service {

    /**
     * CI instance
     *
     * @var object
     */
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->model('Link_model', 'link_model');
    }

    public function get_all()
    {
        return $this->CI->link_model->get_all();
    }

    public function create($title, $url_link)
    {
        $this->CI->link_model->create(array(
            'title' => $title,
            'url_link' => $url_link
        ));
    }

    public function modify($id, $title, $url_link)
    {
        if ($this->CI->link_model->get_by_id($id))
        {
            $this->CI->link_model->modify($id, array(
                'title' => $title,
                'url_link' => $url_link
            ));
        }
    }

    public function delete($id)
    {
        if ($this->CI->link_model->get_by_id($id))
        {
            $this->CI->link_model->delete($id);
        }
    }
}
