<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Category operation
 */
class Category_service {

    /**
     * CI instance
     *
     * @var object
     */
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->model('Category_model', 'category_model');
    }

    /**
     * Create a new child category
     *
     * @param string $title
     * @param int $parent_id
     * @return void
     */
    public function create($title, $parent_id)
    {
        // Confirm the existance of parent category
        $parent = $this->get_by_id($parent_id);
        if ($parent and $parent->is_parent == 1)
        {
            $this->CI->category_model->create(array(
                'title' => $title,
                'parent_id' => $parent_id
            ));
        }
    }

    public function get_by_id($id)
    {
        return $this->CI->category_model->get_by_id($id);
    }

    /**
     * Get categories which are parent category
     *
     * @return array of object
     */
    public function get_pri_cate()
    {
        $rv = $this->CI->category_model->get_parent();
        $category = array();
        if (is_array($rv))
        {
            foreach ($rv as $cate)
            {
                if ($cate->is_parent == 0)
                {
                    array_push($category, array(
                        'category' => $cate,
                        'article_count' => $this->count_article($cate->id)
                    ));
                }
                else
                {
                    array_push($category, array(
                        'category' => $cate
                    ));
                }
            }
            return $category;
        }
        return array();
    }

    /**
     * Get categories which are child category by parent id
     *
     * @param int $id
     * @return array
     */
    public function get_child_cate($id)
    {
        $rv =  $this->CI->category_model->get_by_parent($id);
        $category = array();
        if (is_array($rv))
        {
            foreach ($rv as $cate)
            {
                array_push($category, array(
                    'category' => $cate,
                    'article_count' => $this->count_article($cate->id)
                ));
            }
            return $category;
        }
        return array();
    }

    /**
     * Get all categories
     *
     * @return array
     * Structure:
     * 0 => (
     *         'parent' => category object,
     *         'children'  =>
       [0] =>  (
     *                                'category'      => category object,
     *                                'article_count' => int
     *      ))
     */
    public function get_all()
    {
        $parent_cates = $this->get_pri_cate();
        $category = array();
        foreach ($parent_cates as $parent_cate)
        {
            if ($parent_cate)
            {
                array_push($category, array(
                    'parent' => $parent_cate,
                    'children' => $this->get_child_cate($parent_cate['category']->id)
                ));
            }
        }
        return $category;
    }

    /**
     * Get the sum of article of a category
     *
     * @param int $category_id
     * @return array
     */
    public function count_article($category_id)
    {
        $this->CI->load->model('Article_model', 'article_model');
        $rv = $this->CI->article_model->get_by_category($category_id);
        if (is_array($rv))
        {
            return count($rv);
        }
        return 0;
    }

    public function modify($id, $title)
    {
        if ($this->CI->category_model->get_by_id($id))
        {
            $this->CI->category_model->modify($id, array(
                'title' => $title
            ));
        }
    }

    /**
     * Delete a category
     *
     * @param int $_id
     * @return void
     */
    public function delete($id)
    {
        if ($this->CI->category_model->get_by_id($id))
        {
            $this->CI->category_model->delete($id);
        }
    }
}
