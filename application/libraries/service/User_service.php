<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * User operation
 */
class User_service {

    /**
     * CI instance
     *
     * @var object
     */
    private $CI;

    /**
     * session token 在 cookie 中保存的名称
     *
     * @var string
     */
    private $session_token = 'session_token';

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->model('User_model', 'user_model');
    }

    /**
     * Register a new user
     *
     * @param string $username
     * @param string $password
     * @param string $faculty
     * @param string $mobile_phone
     * @return object|null
     */
    public function register($username, $password)
    {
        if ($this->CI->user_model->check_username($username))
        {
            $this->CI->user_model->create(array(
                'username' => $username,
                'password' => $this->CI->user_model->encrypt_password($username, $password)
            ));
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Get a user from session
     *
     * @return object|null
     */
    public function get_from_session()
    {
        // $this->CI->load->library('session');

        $session = $this->CI->input->cookie($this->session_token);
        return $this->CI->user_model->get_by_session($session);
    }

    /**
     * Attempt login
     *
     * If failed, return null
     *
     * @param string $username
     * @param string $password
     * @return object|null
     */
    public function attempt_login($username, $password)
    {
        $user = $this->CI->user_model->get_by_username($username);

        // 用户不存在
        if (is_null($user))
        {
            return null;
        }

        // 密码不匹配
        if (! $this->CI->user_model->check_password($user, $password))
        {
            return null;
        }

        $this->login($user);

        return $user;
    }

    /**
     * Login a user
     *
     * @param object $user
     * @return void
     */
    public function login($user)
    {
        // $this->CI->load->library('session');

        $session = $this->CI->user_model->create_login_session($user);
        $this->CI->input->set_cookie($this->session_token, $session, 7200);
    }

    /**
     * Logout a user
     *
     * @param object $user
     * @return void
     */
    public function logout($user)
    {
        $this->CI->load->helper('cookie');
        delete_cookie($this->session_token);

        $session = $this->CI->user_model->destory_login_session($user);
    }

    /**
     * Get all users
     *
     * @return array of object
     */
    public function get_all_users()
    {
        return $this->CI->user_model->get_all();
    }

    /**
     * Get user by user's id
     *
     * @param int $id
     * @return object
     */
    public function get_user_by_id($id)
    {
        return $this->CI->user_model->get_by_id($id);
    }

    /**
     * Change user information
     *
     * @param object $user
     * @param array $user_info
     * @return bool
     */
    public function modify($user, $password, $new_password, $user_info)
    {
        if ( ! $this->CI->user_model->check_password($user, $password))
        {
            return false;
        }

        if ($this->CI->user_model->check_username($user_info['username']) or $user_info['username'] == $user->username)
        {
            $this->CI->user_model->modify($user, $user_info);

            // 重新加密密码
            if ($new_password)
            {
                $this->change_password($user, $user_info['username'], $new_password);
            }
            else
            {
                $this->change_password($user, $user_info['username'], $password);
            }
            return true;
        }
        return false;
    }

    /**
     * Change password
     *
     * @param object $user
     * @param string $password
     */
    public function change_password($user, $username, $password)
    {
        $this->CI->user_model->modify($user, array(
            'password' => $this->CI->user_model->encrypt_password($username, $password)
        ));
    }

    public function get_count()
    {
        return $this->CI->user_model->get_count();
    }
}
