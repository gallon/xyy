<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Article operation
 */
class Article_service {

    /**
     * CI instance
     *
     * @var object
     */
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->model('Article_model', 'article_model');
    }

    /**
     * Get article by article's id
     *
     * @param int $id
     * @return object
     */
    public function get_by_id($id)
    {
        return $this->CI->article_model->get_by_id($id);
    }

    public function get_all_by_page($page)
    {
        $rv = $this->get_all();

        $articles = array();
        $each_page = 10;
        $all_count = count($rv);

        $start = ($page-1)*$each_page;
        $end = $page*$each_page;
        for ($i = $start; $i < $end and $i < $all_count; $i++)
        {
            array_push($articles, $rv[$i]);
        }
        return array(
            'articles' => $articles,
            'all_count' => $all_count
        );
    }

    public function get_by_category_and_page($id, $page)
    {
        $rv = $this->get_by_category($id);

        $articles = array();
        $each_page = 10;
        $all_count = count($rv);

        $start = ($page-1)*$each_page;
        $end = $page*$each_page;
        for ($i = $start; $i < $end and $i < $all_count; $i++)
        {
            array_push($articles, $rv[$i]);
        }
        return array(
            'articles' => $articles,
            'all_count' => $all_count
        );
    }

    /**
     * Get articles by category's id
     *
     * @param int $id
     * @return array
     */
    public function get_by_category($category_id)
    {
        $rv = $this->CI->article_model->get_by_category($category_id);
        $articles = array();
        $this->CI->load->model('Category_model', 'category_model');
        $this->CI->load->library('service/user_service', '', 'user_service');
        foreach ($rv as $ev)
        {
            if ($ev)
            {
                array_push($articles, array(
                    'article' => $ev,
                    'category' => $this->CI->category_model->get_by_id($ev->category_id),
                    'author' => $this->CI->user_service->get_user_by_id($ev->author_id)
                ));
            }
        }
        return $articles;
    }

    /**
     * Get all articles
     *
     * @return array
     */
    public function get_all()
    {
        $rv = $this->CI->article_model->get_all();
        $articles = array();
        $this->CI->load->model('Category_model', 'category_model');
        $this->CI->load->library('service/user_service', '', 'user_service');
        foreach ($rv as $ev)
        {
            if ($ev)
            {
                array_push($articles, array(
                    'article' => $ev,
                    'category' => $this->CI->category_model->get_by_id($ev->category_id),
                    'author' => $this->CI->user_service->get_user_by_id($ev->author_id)
                ));
            }
        }
        return $articles;
    }

    public function get_by_parent($id)
    {
        $this->CI->load->model('Category_model', 'category_model');
        $categories = $this->CI->category_model->get_by_parent($id);
        $articles = array();
        foreach ($categories as $cate)
        {
            if ($cate and $cate->parent_id != 0)
            {
                array_push($articles, array(
                    'category' => $cate,
                    'article' => $this->CI->article_model->get_by_category($cate->id)
                ));
            }
        }
        return $articles;
    }

    /**
     * Get articles by categor's id in pages
     *
     * @param int $category_id
     * @param int $page
     * @return array of object
     */
    public function get_in_page($category_id, $page)
    {
        $rv = $this->get_by_category($category_id);
        $articles = array();
        $each_page = 15; // 每页15条文章条目
        $count = 0; // Init counter

        foreach ($rv as $article)
        {
            if ($count > $each_page*($page-1) and $count <= $each_page*$page)
            {
                array_push($articles, $article);
            }
            elseif ($count > $each_page*$page)
            {
                break;
            }
            $count++;
        }
        return $articles;
    }

    public function create($title, $category_id, $content, $author)
    {
        $this->CI->load->model('Category_model', 'category_model');
        // Make sure the existance of category
        if ( ! $this->CI->category_model->get_by_id($category_id))
        {
            return null;
        }
        $article = array(
            'title' => $title,
            'category_id' => $category_id,
            'content' => $content,
            'author_id' => $author->id
        );
        $this->CI->article_model->create($article);
    }

    public function modify($id, $title, $category_id, $content, $author)
    {
        $this->CI->load->model('Category_model', 'category_model');
        // Make sure the existance of category and article
        if ( ! $this->CI->category_model->get_by_id($category_id) or ! $this->get_by_id($id))
        {
            return null;
        }
        $this->CI->load->helper('date');
        $article = array(
            'title' => $title,
            'category_id' => $category_id,
            'content' => $content,
            'author_id' => $author->id
        );
        $this->CI->article_model->modify($id, $article);
    }

    public function delete($id)
    {
        $this->CI->article_model->delete($id);
    }

    public function get_count()
    {
        return $this->CI->article_model->get_count();
    }
}
