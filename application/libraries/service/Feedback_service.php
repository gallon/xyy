<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Feedback operation
 */
class Feedback_service {

    /**
     * CI instance
     *
     * @var object
     */
    private $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->model('Feedback_model', 'feedback_model');
    }

    /**
     * Get feedback by feedback's id
     *
     * @param int $id
     * $return object
     */
    public function get_by_id($id)
    {
        $this->CI->load->library('service/user_service', '', 'user_service');

        $rv = $this->CI->feedback_model->get_by_id($id);

        if ( ! $rv)
        {
            return null;
        }
        return array(
            'feedback' => $rv,
            'handler' => $this->CI->user_service->get_user_by_id($rv->handler_id)
        );
    }

    public function get_all()
    {
        $this->CI->load->library('service/user_service', '', 'user_service');

        $rv = $this->CI->feedback_model->get_all();

        $good_feedback = array();
        $bad_feedback = array();
        foreach ($rv as $feedback)
        {
            if ( ! $feedback)
            {
                break;
            }
            if ($feedback->status == 0)
            {
                array_push($bad_feedback, array(
                    'feedback' => $feedback,
                    'handler' => null
                ));
            }
            else
            {
                array_push($good_feedback, array(
                    'feedback' => $feedback,
                    'handler' => $this->CI->user_service->get_user_by_id($feedback->handler_id)
                ));
            }
        }
        $feedbacks = array(
            'good' => $good_feedback,
            'bad' => $bad_feedback
        );
        return $feedbacks;
    }

    public function get_by_page($page)
    {
        $rv = $this->get_all();
        $good_feedback = $rv['good'];
        $bad_feedback = $rv['bad'];
        foreach ($good_feedback as $feedback)
        {
            array_push($bad_feedback, $feedback);
        }
        $rv = $bad_feedback;

        $feedbacks = array();
        $each_page = 5;
        $all_count = count($rv);

        $start = ($page-1)*$each_page;
        $end = $page*$each_page;
        for ($i = $start; $i < $end and $i < $all_count; $i++)
        {
            array_push($feedbacks, $rv[$i]);
        }
        return array(
            'feedbacks' => $feedbacks,
            'all_count' => $all_count
        );
    }

    public function create($title, $content, $email, $fullname)
    {
        $this->CI->feedback_model->create(array(
            'title' => $title,
            'content' => $content,
            'email' => $email,
            'fullname' => $fullname
        ));
    }
    /**
     * Update comment
     *
     * @param int $id
     * @param string $comment_content
     * @param int $id
     * @return bool | success: true, fail: false
     */
    public function update_comment($id, $comment_content, $handler_id)
    {
        if ($this->get_by_id($id))
        {
            $this->CI->feedback_model->modify($id, array(
                'comment' => $comment_content,
                'handler_id' => $handler_id,
                'status' => 1
            ));
            return true;
        }
        return false;
    }

    /**
     * Delete feedback
     *
     * @param int $id
     * @return bool | success: true, fail: false
     */
    public function delete($id)
    {
        if ($this->get_by_id($id))
        {
            $this->CI->feedback_model->delete($id);
            return true;
        }
        return false;
    }
}
