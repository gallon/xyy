<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'basic/auth_controller.php';

/**
 * Backend feedback controller
 */
class Feedback extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('service/feedback_service', '', 'feedback_service');
    }

    public function index($page = 1)
    {
        $this->load->library('service/feedback_service', '', 'feedback_service');

        $feedbacks =$this->feedback_service->get_by_page($page);
        if ($page > 1 and count($feedbacks) == 0)
        {
            redirect('backend/feedback');
        }
        $this->load_view('backend/parts/header');
        $this->load_view('backend/feedback', array(
            'feedbacks' => $feedbacks['feedbacks'],
            'all_count' => $feedbacks['all_count'],
            'page' => $page
        ));
        $this->load_view('backend/parts/footer');
    }

    public function details($id)
    {
        $feedback = $this->feedback_service->get_by_id($id);
        if ( ! $feedback )
        {
            redirect('backend/feedback');
        }
        $this->load_view('backend/parts/header');
        $this->load_view('backend/feedback_details', array(
            'feedback' => $feedback['feedback'],
            'handler' => $feedback['handler']
        ));
        $this->load_view('backend/parts/footer');
    }
    
    public function update_comment()
    {
        $this->require_methods('POST');

        $id = $this->input->post('id');
        $comment_content = html_escape($this->input->post('comment'));
        $this->feedback_service->update_comment($id, $comment_content, $this->user->id);
        redirect('backend/feedback');
    }

    public function delete($id)
    {
        $this->feedback_service->delete($id);
        redirect('backend/feedback');
    }
}
