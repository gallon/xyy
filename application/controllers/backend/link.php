<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'basic/auth_controller.php';

/**
 * Backend link controller
 */
class Link extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('service/link_service', '', 'link_service');
    }

    public function add()
    {
        $this->require_methods('POST');

        $title = html_escape($this->input->post('title'));
        $url_link = $this->input->post('url');
        if ($title and $url_link)
        {
            $this->link_service->create($title, $url_link);
        }

        redirect('backend/link');
    }

    public function delete($id)
    {
        $this->link_service->delete($id);
        redirect('backend/link');
    }
}
