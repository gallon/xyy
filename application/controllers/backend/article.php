<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'basic/auth_controller.php';

/**
 * Backend article controller
 */
class Article extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('service/article_service', '', 'article_service');
        $this->load->library('service/category_service', '', 'category_service');
    }


    /**
     * Article page
     *
     * @return void
     */
    public function index($page = 1)
    {
        /* for ($j = 5; $j <= 13; $j ++)
           {
           for ($i = 1; $i <= 100; $i ++)
           {
           $this->article_service->create('Test' . $i, $j, 'TestContent', $this->user);
           }
           } */
        $articles = $this->article_service->get_all_by_page($page);
        $this->load_view('backend/parts/header');
        $this->load_view('backend/article', array(
            'page' => $page,
            'page_id' => 0,
            'articles' => $articles['articles'],
            'all_count' => $articles['all_count'],
            'categories' => $this->category_service->get_all()
        ));
        $this->load_view('backend/parts/footer');
    }

    /**
     * Article sort by category page
     *
     * @return void
     */
    public function filter_by_cate($id, $page = 1)
    {
        $articles = $this->article_service->get_by_category_and_page($id, $page);
        $this->load_view('backend/parts/header');
        $this->load_view('backend/article', array(
            'page' => $page,
            'page_id' => $id,
            'articles' => $articles['articles'],
            'all_count' => $articles['all_count'],
            'categories' => $this->category_service->get_all()
        ));
        $this->load_view('backend/parts/footer');
    }
    

    /**
     * Single article page for modify
     *
     * @param int $id
     * @return void
     */
    public function single_article($id)
    {
        $this->load_view('backend/parts/header');
        $this->load_view('backend/single_article', array(
            'article' => $this->article_service->get_by_id($id),
            'categories' => $this->category_service->get_all()
        ));
        $this->load_view('backend/parts/footer');
    }

    /**
     * Add article page
     *
     * @return void
     */
    public function add_article()
    {
        $this->load_view('backend/parts/header');
        $this->load_view('backend/add_article', array(
            'categories' => $this->category_service->get_all()
        ));
        $this->load_view('backend/parts/footer');
    }

    /**
     * Add article action
     *
     * @return void
     */
    public function add()
    {
        $this->require_methods('POST');

        $title = html_escape($this->input->post('title'));
        $category_id = $this->input->post('category');
        $content = $this->input->post('content');
        $this->article_service->create($title, $category_id, $content, $this->user);
        redirect('backend/article');
    }

    /**
     * Modify article action
     *
     * @return void
     */
    public function modify()
    {
        $this->require_methods('POST');

        $id = $this->input->post('article');
        $title = html_escape($this->input->post('title'));
        $category_id = $this->input->post('category');
        $content = $this->input->post('content');
        $this->article_service->modify($id, $title, $category_id, $content, $this->user);
        redirect('backend/article');
    }

    /**
     * Delete article action
     *
     * @return void
     */
    public function delete()
    {
        $this->require_methods('POST');

        $id = $this->input->post('article');
        $this->article_service->delete($id);
        redirect('backend/article');
    }
}
