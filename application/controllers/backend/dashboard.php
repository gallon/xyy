<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'basic/auth_controller.php';

/**
 * Backend dashboard controller
 */
class Dashboard extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Dashboard page
     *
     * @return void
     */
    public function index()
    {
        /* $this->load->library('service/article_service', '', 'article_service');
           for ($i = 10; $i <= 11; $i ++)
           {
           for ($j = 1; $j <= 20; $j ++)
           {
           $this->article_service->create('测试'.$i.$j, $i, '测试'.$j.$i, $this->user);
           }
           } */
        $this->load->library('service/article_service', '', 'article_service');
        $this->load->library('service/user_service', '', 'user_service');
        $this->load->library('service/feedback_service', '', 'feedback_service');
        $this->load_view('backend/parts/header');
        $this->load_view('backend/index', array(
            'article_count' => $this->article_service->get_count(),
            'user_count' => $this->user_service->get_count(),
            'feedbacks' => $this->feedback_service->get_all()
        ));
        $this->load_view('backend/parts/footer');
    }

    /**
     * Introduce page
     *
     * @return void
     */
    public function introduce()
    {
        $this->load->library('service/intro_service', '', 'intro_service');
        $this->load_view('backend/parts/header');
        $this->load_view('backend/introduce', array(
            'introduce' => $this->intro_service->get_one()
        ));
        $this->load_view('backend/parts/footer');
    }

    /**
     * Category page
     *
     * @return void
     */
    public function category()
    {
        $this->load->library('service/category_service', '', 'category_service');

        $this->load_view('backend/parts/header');
        $this->load_view('backend/category', array(
            'categories' => $this->category_service->get_all()
        ));
        $this->load_view('backend/parts/footer');
    }

    public function doctor()
    {
        $this->load->library('service/doctor_service', '', 'doctor_service');

        $this->load_view('backend/parts/header');
        $this->load_view('backend/doctor', array(
            'doctors' => $this->doctor_service->get_all()
        ));
        $this->load_view('backend/parts/footer');
    }

    /**
     * User page
     *
     * @return void
     */
    public function user()
    {
        $this->load->library('service/user_service', '', 'user_service');

        $this->load_view('backend/parts/header');
        $this->load_view('backend/user', array(
            'users' => $this->user_service->get_all_users()
        ));
        $this->load_view('backend/parts/footer');
    }

    public function link()
    {
        $this->load->library('service/link_service', '', 'link_service');

        $this->load_view('backend/parts/header');
        $this->load_view('backend/link', array(
            'links' => $this->link_service->get_all()
        ));
        $this->load_view('backend/parts/footer');
    }
}
