<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'basic/auth_controller.php';

/**
 * Backend dashboard controller
 */
class Campus extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('service/campus_service', '', 'campus_service');
    }

    public function index()
    {
        $this->load_view('backend/parts/header');
        $this->load_view('backend/campus', array(
            'campuses' => $this->campus_service->get_all()
        ));
        $this->load_view('backend/parts/footer');
    }

    public function add()
    {
        $this->require_methods('POST');

        $title = $this->input->post('title');
        $work_time = $this->input->post('work_time');

        if ($title and $work_time)
        {
            $this->campus_service->create($title, $work_time);
        }
        redirect('backend/campus');
    }

    public function modify()
    {
        $this->require_methods('POST');

        $id = $this->input->post('id');
        $title = $this->input->post('title');
        $work_time = $this->input->post('work_time');

        if ($id and $title and $work_time)
        {
            $this->campus_service->modify($id, $title, $work_time);
        }
        redirect('backend/campus');
    }

    public function delete()
    {
        $this->require_methods('POST');

        $id = $this->input->post('id');
        $this->campus_service->delete($id);
        redirect('backend/campus');
    }
}
