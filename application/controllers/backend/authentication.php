<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'basic/base_controller.php';

/**
 * 用户会话管理控制器
 */
class Authentication extends Base_Controller {

    /**
     * 用户登录页
     *
     * @return void
     */
    public function login()
    {
        /* TODO REMOVE register */
        /* $this->user_service->register('root', 'root'); */
        $this->load_view('auth/login');
    }

    /**
     * 验证用户登录用户名及密码
     *
     * @return void
     */
    public function verify_login()
    {
        $this->require_methods('POST');

        // TODO csrf 验证
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $user = $this->user_service->attempt_login($username, $password);

        // 登录失败
        if (is_null($user))
        {
            $this->set_error_message('用户名或密码错误！');
            redirect('user/login');
            return;
        }

        $this->redirect_intended('backend/dashboard');
    }

    /**
     * 用户登出操作
     *
     * @return void
     */
    public function logout()
    {
        $user = $this->user_service->get_from_session();
        $this->user_service->logout($user);

        redirect('user/login');
    }
}
