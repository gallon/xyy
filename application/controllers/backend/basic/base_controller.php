<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 请求停止响应异常
 */
class RequestTerminateException extends Exception {}

/**
 * 控制器基类
 */
class Base_Controller extends CI_Controller {

    /**
     * 操作成功信息键名
     *
     * @var string
     */
    protected $alert_success_key = 'alert_success';

    /**
     * 操作失败信息键名
     *
     * @var string
     */
    protected $alert_error_key = 'alert_error';

    /**
     * session 中试图访问地址键名
     *
     * @var string
     */
    protected $intended_url_key = 'intended_url';

    public function __construct()
    {
        parent::__construct();

        // $this->load->library('session');
        $this->load->library('service/user_service', '', 'user_service');
    }

    /**
     * 重载 remap 来预处理请求
     */
    public function _remap($method, $params = array())
    {
        // 运行预处理操作
        $this->before_request();

        // 调用实际请求响应
        try
        {
            call_user_func_array(array($this, $method), $params);
        }
        catch (RequestTerminateException $e)
        {
            // 忽略请求停止异常，输出响应
            return;
        }
    }

    /**
     * 在正式处理请求前运行的操作
     *
     * @return void
     */
    protected function before_request()
    {
        // 按需重载该方法来使用
    }

    /**
     * 限定 controller 响应的 HTTP 请求方法
     *
     * 如果当前请求方法不符合需要的方法，返回 405.
     *
     * @param array|string $methods
     * @return void
     */
    protected function require_methods($methods)
    {
        if (is_string($methods))
        {
            $methods = array($methods);
        }

        $request_method = $this->input->server('REQUEST_METHOD');
        foreach ($methods as $allowed_method)
        {
            // 请求符合对应方法，返回。
            if (strtolower($request_method) === strtolower($allowed_method))
            {
                return;
            }
        }

        // 请求非法，返回 405 响应。
        $this->output->set_status_header(405);
        throw new RequestTerminateException('invalid request method');
    }

    /**
     * 填充 view 默认渲染数据
     *
     * @param array $payload
     * @return array
     */
    protected function view_data($payload = array())
    {
        $this->load->helper('cookie');
        $default = array();

        $alert_error = $this->input->cookie($this->alert_error_key);
        delete_cookie($this->alert_error_key);
        if ($alert_error)
        {
            $default[$this->alert_error_key] = $alert_error;
        }

        $alert_success = $this->input->cookie($this->alert_success_key);
        delete_cookie($this->alert_success_key);
        if ($alert_success)
        {
            $default[$this->alert_success_key] = $alert_success;
        }

        // 确保后来提供的数据不会被覆盖
        return array_merge($default, $payload);
    }

    /**
     * 加载一个 view 并使用默认填充数据
     *
     * @param string $view
     * @param array $payload
     * @return void
     */
    protected function load_view($view, $payload = array())
    {
        // return $this->load->view($view, $payload);
        return $this->load->view($view, $this->view_data($payload));
    }

    /**
     * 设置 flash message 到 session 中
     *
     * @param string $category
     * @param string $message
     * @return void
     */
    protected function set_flash_message($category, $message)
    {
        $this->input->set_cookie($category, $message, 0);
        // $this->session->set_flashdata($category, $message);
    }

    /**
     * 设置操作成功(alert_success)的 flash message
     *
     * @param string $message
     * @return void
     */
    protected function set_success_message($message)
    {
        $this->set_flash_message($this->alert_success_key, $message);
    }

    /**
     * 设置操作失败(alert_error)的 flash message
     *
     * @param string $message
     * @return void
     */
    protected function set_error_message($message)
    {
        $this->set_flash_message($this->alert_error_key, $message);
    }

    /**
     * 记录未登录用户试图访问的地址到 session 中
     *
     * @param string $url
     * @return void
     */
    protected function set_intended_url($url)
    {
        // $this->load->library('session');
        // $this->session->set_userdata($this->intended_url_key, $url);
        $this->input->set_cookie($this->intended_url_key, $url, 0);
    }

    /**
     * 引导登录后的用户到登录前试图访问的地址
     *
     * 如果没有记录试图访问的地址，引导到 $fallback 指向的地址
     *
     * @param string $fallback
     * @return void
     */
    protected function redirect_intended($fallback = '/')
    {
        // $this->load->library('session');
        // $intended_url = $this->session->userdata($this->intended_url_key);
        $this->load->helper('cookie');
        $intended_url = $this->input->cookie($this->intended_url_key);
        delete_cookie($this->intended_url_key);

        if (! $intended_url)
        {
            $intended_url = $fallback;
        }

        redirect($intended_url, 'location', 301);
    }
}
