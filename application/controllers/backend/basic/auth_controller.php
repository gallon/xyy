<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'base_controller.php';

/**
 * 限制未登录用户访问控制器基类
 */
class Auth_Controller extends Base_Controller {

    /**
     * 当前会话用户实例
     *
     * @var object
     */
    protected $user;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 在处理请求前检查当前用户是否已经登录
     *
     * 如果没有，引导到登录页面。
     *
     * @return void
     */
    protected function require_user_login()
    {
        $this->user = $this->user_service->get_from_session();

        // 当前用户没有登录，跳转到登录页面
        if (is_null($this->user))
        {
            $this->set_error_message('请先进行登录');
            $this->set_intended_url(current_url());

            redirect('/user/login', 'location', 301);
        }
    }

    /**
     * 检查用户登录状态
     *
     * @return void
     */
    protected function before_request()
    {
        // 检查用户是否登录
        $this->require_user_login();

        // 保证执行继承的其他操作
        parent::before_request();
    }

    /**
     * 填充 view 默认渲染数据
     *
     * @param array $payload
     * @return array
     */
    protected function view_data($payload = array())
    {
        $data = parent::view_data($payload);

        if (! isset($data['user']))
        {
            $data['user'] = $this->user;
        }

        return $data;
    }
}
