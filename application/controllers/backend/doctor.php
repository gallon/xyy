<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'basic/auth_controller.php';

/**
 * Backend doctors controller
 */
class Doctor extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('service/doctor_service', '', 'doctor_service');
    }

    public function add_doctor()
    {
        $this->load_view('backend/parts/header');
        $this->load_view('backend/add_doctor');
        $this->load_view('backend/parts/footer');
    }

    public function details($id)
    {
        $this->load_view('backend/parts/header');
        $this->load_view('backend/doctor_details', array(
            'doctor' => $this->doctor_service->get_by_id($id)
        ));
        $this->load_view('backend/parts/footer');
    }

    public function add()
    {
        $this->require_methods('POST');

        $fullname = html_escape($this->input->post('fullname'));
        $position = html_escape($this->input->post('position'));
        $campus = html_escape($this->input->post('campus'));
        $phone = html_escape($this->input->post('phone'));
        $portrait = $this->input->post('portrait');
        $description = html_escape($this->input->post('description'));
        $expert = $this->input->post('expert');

        $this->doctor_service->create(array(
            'fullname' => $fullname,
            'position' => $position,
            'campus' => $campus,
            'phone' => $phone,
            'portrait' => $portrait,
            'description' => $description,
            'expert' => $expert
        ));
        redirect('backend/doctor');
    }

    public function modify()
    {
        $this->require_methods('POST');

        $id = $this->input->post('id');
        $fullname = html_escape($this->input->post('fullname'));
        $position = html_escape($this->input->post('position'));
        $campus = html_escape($this->input->post('campus'));
        $phone = html_escape($this->input->post('phone'));
        $portrait = $this->input->post('portrait');
        $description = html_escape($this->input->post('description'));
        $expert = $this->input->post('expert');

        $doctor = $this->doctor_service->get_by_id($id);

        if ($doctor)
        {
            $this->doctor_service->modify($doctor, array(
                'fullname' => $fullname,
                'position' => $position,
                'campus' => $campus,
                'phone' => $phone,
                'portrait' => $portrait,
                'description' => $description,
                'expert' => $expert
            ));
        }
        redirect('backend/dashboard/doctor');
    }

    public function delete($id)
    {
        $this->doctor_service->delete($id);
        redirect('backend/dashboard/doctor');
    }
}
