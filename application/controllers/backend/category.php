<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'basic/auth_controller.php';

/**
 * Backend category controller
 */
class Category extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('service/category_service', '', 'category_service');
    }

    /**
     * Add category action
     *
     * @return void
     */
    public function add()
    {
        $this->require_methods('POST');

        $title = html_escape($this->input->post('title'));
        $parent = $this->input->post('parent-id');
        $this->category_service->create($title, $parent);
        redirect('backend/category');
    }


    public function modify()
    {
        $this->require_methods('POST');

        $title = html_escape($this->input->post('category-title'));
        $id = $this->input->post('category-id');

        $this->category_service->modify($id, $title);
        redirect('backend/category');
    }
    
    public function delete()
    {
        $this->require_methods('POST');

        $id = $this->input->post('category-id');

        $category = $this->category_service->get_by_id($id);
        if ($category)
        {
            $article_quantity = $this->category_service->count_article($id);
            if ($article_quantity > 0)
            {
                $this->set_error_message($category->title . '分类仍存在'. $article_quantity .'篇文章，无法删除此分类');
            }
            else
            {
                $this->category_service->delete($id);
                $this->set_success_message('操作成功');
            }
        }
        else
        {
            $this->set_error_message('无效操作');
        }
        redirect('backend/category');
    }
}
