<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'basic/auth_controller.php';

/**
 * Backend introduce controller
 */
class Introduce extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('service/intro_service', '', 'intro_service');
    }

    /**
     * Modify introduce action
     *
     * @return void
     */
    public function modify()
    {
        $this->require_methods('POST');

        $title = html_escape($this->input->post('title'));
        $content = $this->input->post('content');

        $this->intro_service->modify($title, $content);
        redirect('backend/introduce');
    }
}
