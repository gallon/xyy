<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once 'basic/auth_controller.php';

/**
 * Backend users controller
 */
class User extends Auth_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('service/user_service', '', 'user_service');
    }

    public function add()
    {
        $this->require_methods('POST');

        $username = html_escape($this->input->post('username'));
        $password = $this->input->post('password');

        $status = $this->user_service->register($username, $password);

        if ($status === false)
        {
            $this->set_error_message('用户名已存在');
        }
        else
        {
            $this->set_success_message('注册成功');
        }
        redirect('backend/users');
    }

    public function modify()
    {
        $this->require_methods('POST');

        $username = html_escape($this->input->post('username'));
        $password = $this->input->post('password');
        $new_password = $this->input->post('new-password');
        $password_check = $this->input->post('password-check');

        if ($username and $new_password === $password_check)
        {
            $this->user_service->modify($this->user, $password,
                                        $new_password, array(
                    'username' => $username
                ));
        }
        redirect('backend/dashboard');
    }
}
