<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Introduce extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('service/article_service', '', 'article_service');
        $this->load->library('service/category_service', '', 'category_service');
        $this->load->library('service/link_service', '', 'link_service');

        $this->intro = $this->category_service->get_child_cate(1);
    }

    public function hospital()
    {
        $this->load->library('service/intro_service', '', 'intro_service');

        $this->load->view('frontend/parts/header', array(
            'page_nav' => 2,  // default value
            'categories' => $this->category_service->get_all(),
            'guides' => $this->article_service->get_by_category(5),
            'links' => $this->link_service->get_all()
        ));
        $this->load->view('frontend/hospital', array(
            'intro_cates' => $this->intro,
            'introduce_article' => $this->intro_service->get_one()
        ));
        $this->load->view('frontend/parts/footer');
    }

    public function doctor_details($id)
    {
        $this->load->library('service/intro_service', '', 'intro_service');
        $this->load->library('service/doctor_service', '', 'doctor_service');

        $doctor = $this->doctor_service->get_by_id($id);
        if ( ! $doctor)
        {
            redirect('intro/doctor');
        }
        $this->load->view('frontend/parts/header', array(
            'page_nav' => 2,  // default value
            'categories' => $this->category_service->get_all(),
            'guides' => $this->article_service->get_by_category(5),
            'links' => $this->link_service->get_all()
        ));
        $this->load->view('frontend/doctor_details', array(
            'intro_cates' => $this->intro,
            'doctor' => $doctor
        ));
        $this->load->view('frontend/parts/footer');
    }

    public function expert()
    {
        $this->load->library('service/doctor_service', '', 'doctor_service');

        $this->load->view('frontend/parts/header', array(
            'page_nav' => 2,  // default value
            'categories' => $this->category_service->get_all(),
            'guides' => $this->article_service->get_by_category(5),
            'links' => $this->link_service->get_all()
        ));
        $this->load->view('frontend/expert', array(
            'intro_cates' => $this->intro,
            'experts' => $this->doctor_service->get_expert()
        ));
        $this->load->view('frontend/parts/footer');
    }

    public function doctor()
    {
        $this->load->library('service/doctor_service', '', 'doctor_service');

        $this->load->view('frontend/parts/header', array(
            'page_nav' => 2,  // default value
            'categories' => $this->category_service->get_all(),
            'guides' => $this->article_service->get_by_category(5),
            'links' => $this->link_service->get_all()
        ));
        $this->load->view('frontend/doctor', array(
            'intro_cates' => $this->intro,
            'doctors' => $this->doctor_service->get_all()
        ));
        $this->load->view('frontend/parts/footer');
    }

    public function other_cate($id, $page = 1)
    {
        $this->load->library('service/category_service', '', 'category_service');
        $this->load->library('service/article_service', '', 'article_service');

        $this_category = null;
        foreach ($this->intro as $category)
        {
            if ($category['category']->id == $id)
            {
                $this_category = $category['category'];
                break;
            }
        }
        if ( ! $this_category)
        {
            redirect('/');
        }
        else
        {
            $this->load->view('frontend/parts/header', array(
                'page_nav' => 2,  // default value
                'categories' => $this->category_service->get_all(),
                'guides' => $this->article_service->get_by_category(5),
                'links' => $this->link_service->get_all()
            ));
            $articles = $this->article_service->get_by_category_and_page($this_category->id, $page);
            $this->load->view('frontend/intro_cate', array(
                'intro_cates' => $this->intro,
                'articles' => $articles,
                'page_title' => $this_category->title,
                'page_id' => $id,
                'page' => $page
            ));
            $this->load->view('frontend/parts/footer');
        }
    }
}
