<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Home page controller
 */
class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->library('service/article_service', '', 'article_service');
        $this->load->library('service/category_service', '', 'category_service');
        $this->load->library('service/link_service', '', 'link_service');
    }

    /**
     * Home Page
     *
     * @return void
     */
    public function index()
    {
        $articles = array();
        $articles['intro'] = $this->article_service->get_by_parent(1);
        $articles['news'] = $this->article_service->get_by_parent(2);
        $articles['exchange'] = $this->article_service->get_by_parent(4);
        $articles['guide'] = $this->article_service->get_by_parent(5);
        $articles['department'] = $this->article_service->get_by_category(6);
        $articles['social'] = $this->article_service->get_by_category(7);

        $articles['health_1'] = $this->article_service->get_by_category(8);  // 预防保健
        $articles['health_2'] = $this->article_service->get_by_category(9);  // 健康教育

        // Doctors and experts
        $this->load->library('service/doctor_service', '', 'doctor_service');
        $doctors = $this->doctor_service->get_all();
        $experts = $this->doctor_service->get_expert();

        // Campus work time
        $this->load->library('service/campus_service', '', 'campus_service');
        $campuses = $this->campus_service->get_all();

        $this->load->library('service/feedback_service', '', 'feedback_service');
        $this->load->view('frontend/parts/header', array(
            'page_nav' => 1,  // default value
            'categories' => $this->category_service->get_all(),
            'guides' => $this->article_service->get_by_category(5),
            'links' => $this->link_service->get_all()
        ));
        $this->load->view('frontend/index', array(
            'articles' => $articles,
            'doctors' => $doctors,
            'experts' => $experts,
            'campuses' => $campuses,
            'feedbacks' => $this->feedback_service->get_all()
        ));
        $this->load->view('frontend/parts/footer');
    }

    public function news($id, $page = 1)
    {
        $news = $this->category_service->get_child_cate(2);
        $this_category = null;
        foreach ($news as $category)
        {
            if ($category['category']->id == $id)
            {
                $this_category = $category['category'];
                break;
            }
        }
        if ( ! $this_category)
        {
            redirect('/');
        }
        else
        {
            $this->load->view('frontend/parts/header', array(
                'page_nav' => 3,  // default value
                'categories' => $this->category_service->get_all(),
                'guides' => $this->article_service->get_by_category(5),
                'links' => $this->link_service->get_all()
            ));
            $articles = $this->article_service->get_by_category_and_page($this_category->id, $page);
            $this->load->view('frontend/newslist', array(
                'news_cates' => $news,
                'articles' => $articles['articles'],
                'all_count' => $articles['all_count'],
                'page_title' => $this_category->title,
                'page_id' => $id,
                'page' => $page
            ));
            $this->load->view('frontend/parts/footer');
        }
    }

    public function department($page = 1)
    {
        $articles = $this->article_service->get_by_category_and_page(6, $page);
        $this->load->view('frontend/parts/header', array(
            'page_nav' => 5,  // default value
            'categories' => $this->category_service->get_all(),
            'guides' => $this->article_service->get_by_category(5),
            'links' => $this->link_service->get_all()
        ));
        $this->load->view('frontend/department', array(
            'articles' => $articles['articles'],
            'all_count' => $articles['all_count'],
            'page' => $page
        ));
        $this->load->view('frontend/parts/footer');
    }

    public function guide($id)
    {
        $article = null;
        $article = $this->article_service->get_by_id($id);
        if ( ! $article)
        {
            redirect('/');
        }
        $this->load->view('frontend/parts/header', array(
            'page_nav' => 4,  // default value
            'categories' => $this->category_service->get_all(),
            'guides' => $this->article_service->get_by_category(5),
            'links' => $this->link_service->get_all()
        ));
        $this->load->view('frontend/guide', array(
            'article' => $article
        ));
        $this->load->view('frontend/parts/footer');
    }

    public function guide_traffic()
    {
        $this->load->view('frontend/parts/header', array(
            'page_nav' => 4,  // default value
            'categories' => $this->category_service->get_all(),
            'guides' => $this->article_service->get_by_category(5),
            'links' => $this->link_service->get_all()
        ));
        $this->load->view('frontend/guide_traffic');
        $this->load->view('frontend/parts/footer');
    }

    public function health($id, $page = 1)
    {
        $health = $this->category_service->get_child_cate(3);
        $this_category = null;
        foreach ($health as $category)
        {
            if ($category['category']->id == $id)
            {
                $this_category = $category['category'];
                break;
            }
        }
        if ( ! $this_category)
        {
            redirect('/');
        }
        else
        {
            $this->load->view('frontend/parts/header', array(
                'page_nav' => 6,  // default value
                'categories' => $this->category_service->get_all(),
                'guides' => $this->article_service->get_by_category(5),
                'links' => $this->link_service->get_all()
            ));
            $articles = $this->article_service->get_by_category_and_page($this_category->id, $page);
            $this->load->view('frontend/health', array(
                'health_cates' => $health,
                'articles' => $articles['articles'],
                'all_count' => $articles['all_count'],
                'page_title' => $this_category->title,
                'page_id' => $id,
                'page' => $page
            ));
            $this->load->view('frontend/parts/footer');
        }
    }

    public function social($page = 1)
    {
        $articles = $this->article_service->get_by_category_and_page(7, $page);
        $this->load->view('frontend/parts/header', array(
            'page_nav' => 7,  // default value
            'categories' => $this->category_service->get_all(),
            'guides' => $this->article_service->get_by_category(5),
            'links' => $this->link_service->get_all()
        ));
        $this->load->view('frontend/social', array(
            'articles' => $articles['articles'],
            'all_count' => $articles['all_count'],
            'page' => $page
        ));
        $this->load->view('frontend/parts/footer');
    }

    public function feedback($page = 1)
    {
        $this->load->library('service/feedback_service', '', 'feedback_service');

        $feedbacks = $this->feedback_service->get_by_page($page);
        $this->load->view('frontend/parts/header', array(
            'page_nav' => 8,  // default value
            'categories' => $this->category_service->get_all(),
            'guides' => $this->article_service->get_by_category(5),
            'links' => $this->link_service->get_all()
        ));
        $this->load->view('frontend/feedback', array(
            'feedbacks' => $feedbacks['feedbacks'],
            'all_count' => $feedbacks['all_count'],
            'exchange_cates' => $this->category_service->get_child_cate(4),
            'page' => $page
        ));
        $this->load->view('frontend/parts/footer');
    }

    public function feedback_details($id)
    {
        $this->load->library('service/feedback_service', '', 'feedback_service');
        $feedback = $this->feedback_service->get_by_id($id);
        if ( ! $feedback)
        {
            redirect('feedback');
        }
        $this->load->view('frontend/parts/header', array(
            'page_nav' => 8,  // default value
            'categories' => $this->category_service->get_all(),
            'guides' => $this->article_service->get_by_category(5),
            'links' => $this->link_service->get_all()
        ));
        $this->load->view('frontend/feedback_details', array(
            'feedback' => $feedback['feedback']
        ));
        $this->load->view('frontend/parts/footer');
    }

    public function add_feedback()
    {
        $this->load->library('service/feedback_service', '', 'feedback_service');

        $fullname = html_escape($this->input->post('fullname'));
        $email = html_escape($this->input->post('email'));
        $title = html_escape($this->input->post('title'));
        $content = html_escape($this->input->post('content'));
        if ($fullname and $email and $title and $content)
        {
            $this->feedback_service->create($title, $content, $email, $fullname);
        }
        redirect('feedback');
    }

    public function exchange($id, $page = 1)
    {
        $exchange = $this->category_service->get_child_cate(4);
        $this_category = null;
        foreach ($exchange as $category)
        {
            if ($category['category']->id == $id)
            {
                $this_category = $category['category'];
                break;
            }
        }
        if ( ! $this_category)
        {
            redirect('/');
        }
        else
        {
            $this->load->view('frontend/parts/header', array(
                'page_nav' => 8,  // default value
                'categories' => $this->category_service->get_all(),
                'guides' => $this->article_service->get_by_category(5),
                'links' => $this->link_service->get_all()
            ));
            $articles = $this->article_service->get_by_category_and_page($this_category->id, $page);
            $this->load->view('frontend/exchange', array(
                'exchange_cates' => $exchange,
                'articles' => $articles['articles'],
                'all_count' => $articles['all_count'],
                'page_title' => $this_category->title,
                'page_id' => $id,
                'page' => $page
            ));
            $this->load->view('frontend/parts/footer');
        }
    }

    public function article($id)
    {
        $page_nav =null;
        $article = $this->article_service->get_by_id($id);
        if ( ! $article)
        {
            redirect('/');
        }
        $category = $this->category_service->get_by_id($article->category_id);
        if ($category->id > 7)
        {
            $parent_cate = $this->category_service->get_by_id($category->parent_id);
            $url = null;
            switch ($parent_cate->id)
                {
                case 1:
                    $url = 'intro';
                    $page_nav = 2;
                    break;
                case 2:
                    $url = 'news';
                    $page_nav = 3;
                    break;
                case 3:
                    $url = 'health';
                    $page_nav = 6;
                    break;
                case 4:
                    $url = 'exchange';
                    $page_nav = 8;
                    break;
                }
            $this_page = array(
                'url' => $url . '/' . $category->id,
                'title' => $category->title,
                'parent' => $parent_cate->title
            );
        }
        elseif ($category->id == 6 or $category->id == 7)
        {
            $parent_cate = $category;
            $this_page = array(
                'url' => $category->id == 6 ? 'department' : 'social',
                'title' => $category->title,
                'parent' => $parent_cate->title
            );
            $page_nav = $category->id == 6 ? 5 : 7;
        }
        $this->load->view('frontend/parts/header', array(
            'page_nav' => $page_nav,
            'categories' => $this->category_service->get_all(),
            'guides' => $this->article_service->get_by_category(5),
            'links' => $this->link_service->get_all()
        ));
        $this->load->view('frontend/article', array(
            'this_page' => $this_page,
            'article' => $article
        ));
        $this->load->view('frontend/parts/footer');
    }
}
